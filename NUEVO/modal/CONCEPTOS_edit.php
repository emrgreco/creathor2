<div class="modal fade" id="CONCEPTOS_modal_edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title" id="myModalLabel"><i class="fas fa-xs fa-bell"></i> EDITAR CONCEPTOS</h4>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<div class="modal-body">
						<form class="form-horizontal" data-async data-target="#rating-modal" method="post" id="CONCEPTOS_edit_form" name="CONCEPTOS_edit_form" action="../actions/CONCEPTOS_edit_action.php" enctype="multipart/form-data">
							<div id="CONCEPTOS_resultados_modal_edit"></div><div class="form-group">
										<input type="hidden" class="form-control" id="edit_CONCEPTOS_ID_indice" name="edit_CONCEPTOS_ID_indice" placeholder="AS,L" value=""  required	>
									</div>									<div class="form-group">
													<label for="for_CLAVE_campo" class="control-label">CLAVE</label>
													<input type="text" class="form-control" id="edit_CONCEPTOS_CLAVE_campo" name="edit_CONCEPTOS_CLAVE_campo" placeholder="KAMSLAKM" value="" required >
									</div>
									<div class="form-group">
													<label for="for_PRECIO_campo" class="control-label">PRECIO</label>
													<input type="number" value="" maxlength="10" step="0.0000001" pattern="^[0-9]{1,5}(\.[0-9]{0,2})?$" class="form-control" id="edit_CONCEPTOS_PRECIO_campo" name="edit_CONCEPTOS_PRECIO" placeholder="SALKMA" value="" >
									</div>
									<div class="form-group">
													<label for="for_UNIDAD_campo" class="control-label">UNIDAD</label>
													<input type="text" class="form-control" id="edit_CONCEPTOS_UNIDAD_campo" name="edit_CONCEPTOS_UNIDAD_campo" placeholder="ASAS" value="" >
									</div>
									<div class="form-group">
													<label for="for_ARCHIVOS_campo" class="control-label">ARCHIVOS</label>
													<input type="text" class="form-control" id="edit_CONCEPTOS_ARCHIVOS_campo" name="edit_CONCEPTOS_ARCHIVOS_campo" placeholder="ARCHIVOS" value="" >
									</div>
									<div class="form-group">
													<label for="for_PRUEBA_campo" class="control-label">PRUEBA</label>

	                        						<img id="edit_img_CONCEPTOS_PRUEBA_campo"src="<?php echo "../HOST/$PRUEBA"; ?>" width="100">
													<input type="file" multiple="multiple" class="form-control" id="edit_CONCEPTOS_PRUEBA_campo" name="edit_CONCEPTOS_PRUEBA_campo" value="" accept=".jpeg, .jpg, .gif, .eps, .pdf, .dwg, .txt, .rar" value="" >
									</div>
<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
								<button type="submit" class="btn btn-primary" id="CONCEPTOS_actualizar_datos">Actualizar datos</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>