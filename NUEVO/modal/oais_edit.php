<div class="modal fade" id="oais_modal_edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title" id="myModalLabel"><i class="fas fa-xs fa-plus"></i> EDITAR oais</h4>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<div class="modal-body">
						<form class="form-horizontal" data-async data-target="#rating-modal" method="post" id="oais_edit_form" name="oais_edit_form" >
							<div id="oais_resultados_modal_edit"></div><div class="form-group">
										<input type="hidden" class="form-control" id="edit_oais_id_indice" name="edit_oais_id_indice" placeholder="id" value="" 	>
									</div><div class="form-group">
											<input type="hidden" class="form-control" id="edit_oais_ID_CONCEPTOS_externo" name="edit_oais_ID_CONCEPTOS_externo" placeholder="AS,L" value=""  required	>
										</div>									<div class="form-group">
													<label for="for_dos_campo" class="control-label">dos</label>
													<input type="text" class="form-control" id="edit_oais_dos_campo" name="edit_oais_dos_campo" placeholder="dos" value="" required >
									</div>
<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
								<button type="submit" class="btn btn-primary" id="oais_actualizar_datos">Actualizar datos</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>