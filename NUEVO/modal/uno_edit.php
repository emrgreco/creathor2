<div class="modal fade" id="uno_modal_edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title" id="myModalLabel"><i class="fas fa-xs fa-plus"></i> EDITAR uno</h4>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<div class="modal-body">
						<form class="form-horizontal" data-async data-target="#rating-modal" method="post" id="uno_edit_form" name="uno_edit_form" action="../actions/uno_edit_action.php" enctype="multipart/form-data">
							<div id="uno_resultados_modal_edit"></div><div class="form-group">
										<input type="hidden" class="form-control" id="edit_uno_id_indice" name="edit_uno_id_indice" placeholder="id" value="" 	>
									</div><div class="form-group">
											<input type="hidden" class="form-control" id="edit_uno_ID_CONCEPTOS_externo" name="edit_uno_ID_CONCEPTOS_externo" placeholder="AS,L" value=""  required	>
										</div>									<div class="form-group">
													<label for="for_dos_campo" class="control-label">dos</label>

	                        						<img id="edit_img_uno_dos_campo"src="<?php echo "../uploads/$dos"; ?>" width="100">
													<input type="file" multiple="multiple" class="form-control" id="edit_uno_dos_campo" name="edit_uno_dos_campo" value="" accept=".jpeg, .jpg" value="" >
									</div>
									<div class="form-group">
													<label for="for_tres_campo" class="control-label">tres</label>
													<input type="text" class="form-control" id="edit_uno_tres_campo" name="edit_uno_tres_campo" placeholder="tres" value="" >
									</div>
									<div class="form-group">
													<label for="for_OTRAIMAGEN_campo" class="control-label">OTRAIMAGEN</label>

	                        						<img id="edit_img_uno_OTRAIMAGEN_campo"src="<?php echo "../wakandas/$OTRAIMAGEN"; ?>" width="100">
													<input type="file" multiple="multiple" class="form-control" id="edit_uno_OTRAIMAGEN_campo" name="edit_uno_OTRAIMAGEN_campo" value="" accept=".png" value="" >
									</div>
									<div class="form-group">
													<label for="for_OTRO_campo" class="control-label">OTRO</label>
													<input type="checkbox" class="form-control" id="edit_uno_OTRO_campo" name="edit_uno_OTRO_campo" placeholder="OTRO" value="" >
									</div>
									<div class="form-group">
													<label for="for_MASIMAGEN_campo" class="control-label">MASIMAGEN</label>

	                        						<img id="edit_img_uno_MASIMAGEN_campo"src="<?php echo "../OTRAMAS/$MASIMAGEN"; ?>" width="100">
													<input type="file" multiple="multiple" class="form-control" id="edit_uno_MASIMAGEN_campo" name="edit_uno_MASIMAGEN_campo" value="" accept=".gif" value="" >
									</div>
<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
								<button type="submit" class="btn btn-primary" id="uno_actualizar_datos">Actualizar datos</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>