<div class="modal fade" id="CONCEPTOS_modal_add" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title" id="CONCEPTOS_modal_addLabel"><i class="fas fa-xs fa-bell"></i> NUEVO CONCEPTOS</h4>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<div class="modal-body">
						<form class="form-horizontal" data-async data-target="#rating-modal" method="post" id="CONCEPTOS_add_form" name="CONCEPTOS_add_form" action="../actions/CONCEPTOS_add_action.php" enctype="multipart/form-data">
							<div id="CONCEPTOS_resultados_modal_add"></div>
									<div class="form-group">
													<label for="for_CLAVE" class="control-label">CLAVE</label>
													<input type="text" class="form-control" id="add_CONCEPTOS_CLAVE_campo" name="add_CONCEPTOS_CLAVE_campo" placeholder="KAMSLAKM" required >
									</div>
									<div class="form-group">
													<label for="for_PRECIO" class="control-label">PRECIO</label>
													<input type="number" value="" maxlength="10" step="0.0000001" pattern="^[0-9]{1,5}(\.[0-9]{0,2})?$" class="form-control" id="add_CONCEPTOS_PRECIO_campo" name="add_CONCEPTOS_PRECIO_campo" placeholder="SALKMA"  >
									</div>
									<div class="form-group">
													<label for="for_UNIDAD" class="control-label">UNIDAD</label>
													<input type="text" class="form-control" id="add_CONCEPTOS_UNIDAD_campo" name="add_CONCEPTOS_UNIDAD_campo" placeholder="ASAS" >
									</div>
									<div class="form-group">
													<label for="for_ARCHIVOS" class="control-label">ARCHIVOS</label>
													<input type="text" class="form-control" id="add_CONCEPTOS_ARCHIVOS_campo" name="add_CONCEPTOS_ARCHIVOS_campo" placeholder="ARCHIVOS" >
									</div>
									<div class="form-group">
													<label for="for_PRUEBA" class="control-label">PRUEBA</label>

	                        						<img id="img_PRUEBA"src="<?php echo "../HOST/$PRUEBA"; ?>" width="100">
													<input type="file" class="form-control" id="add_CONCEPTOS_PRUEBA_campo" name="add_CONCEPTOS_PRUEBA_campo" value="" accept=".jpeg, .jpg, .gif, .eps, .pdf, .dwg, .txt, .rar" >
									</div>
									<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
								<button type="submit" class="btn btn-primary" id="CONCEPTOS_guardar_datos">Guardar datos</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>