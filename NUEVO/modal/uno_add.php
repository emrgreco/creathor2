<div class="modal fade" id="uno_modal_add" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title" id="uno_modal_addLabel"><i class="fas fa-xs fa-plus"></i> NUEVO uno</h4>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<div class="modal-body">
						<form class="form-horizontal" data-async data-target="#rating-modal" method="post" id="uno_add_form" name="uno_add_form" action="../actions/uno_add_action.php" enctype="multipart/form-data">
							<div id="uno_resultados_modal_add"></div>
<div class="form-group">
											<input type="hidden" class="form-control" id="add_uno_ID_CONCEPTOS_externo" name="add_uno_ID_CONCEPTOS_externo" placeholder="AS,L" required	value="<?php echo $_GET["CONCEPTOS"];?>">
										</div>									<div class="form-group">
													<label for="for_dos" class="control-label">dos</label>

	                        						<img id="img_dos"src="<?php echo "../uploads/$dos"; ?>" width="100">
													<input type="file" class="form-control" id="add_uno_dos_campo" name="add_uno_dos_campo" value="" accept=".jpeg, .jpg" >
									</div>
									<div class="form-group">
													<label for="for_tres" class="control-label">tres</label>
													<input type="text" class="form-control" id="add_uno_tres_campo" name="add_uno_tres_campo" placeholder="tres" >
									</div>
									<div class="form-group">
													<label for="for_OTRAIMAGEN" class="control-label">OTRAIMAGEN</label>

	                        						<img id="img_OTRAIMAGEN"src="<?php echo "../wakandas/$OTRAIMAGEN"; ?>" width="100">
													<input type="file" class="form-control" id="add_uno_OTRAIMAGEN_campo" name="add_uno_OTRAIMAGEN_campo" value="" accept=".png" >
									</div>
									<div class="form-group">
													<label for="for_OTRO" class="control-label">OTRO</label>
													<input type="checkbox" class="form-control" id="add_uno_OTRO_campo" name="add_uno_OTRO_campo" placeholder="OTRO" >
									</div>
									<div class="form-group">
													<label for="for_MASIMAGEN" class="control-label">MASIMAGEN</label>

	                        						<img id="img_MASIMAGEN"src="<?php echo "../OTRAMAS/$MASIMAGEN"; ?>" width="100">
													<input type="file" class="form-control" id="add_uno_MASIMAGEN_campo" name="add_uno_MASIMAGEN_campo" value="" accept=".gif" >
									</div>
									<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
								<button type="submit" class="btn btn-primary" id="uno_guardar_datos">Guardar datos</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>