
		<?php header("Access-Control-Allow-Origin", "*"); 

		session_start();
	    if(isset($_SESSION["tiempo"]) ) {

	        $inactivo = 1200;//20min en este caso.

	        $vida_session = time() - $_SESSION["tiempo"];

	            if($vida_session > $inactivo)
	            {
	                //Removemos sesión.
	                session_unset();
	                //Destruimos sesión.
	                session_destroy();              
	                //Redirigimos pagina.
	                header("Location: index.php");
	                exit();
	            }

	    }
	    $_SESSION["tiempo"] = time();
	    //echo "<pre>";
	    //var_dump($_SESSION);
	    //echo "</pre>";

	    ?>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<title><?php echo $title;?></title>
		<style>"<?php include(dirname(__FILE__). "./css/bootstrap.min.css"); ?>"</style>
		<style>"<?php include(dirname(__FILE__). "./css/all.min.css"); ?>"</style>
		<style sizes="32x32" type="image/png">"<?php include(dirname(__FILE__). "./img/logo-icon.png"); ?>"</style>
		<style>"<?php include(dirname(__FILE__). "./css/datepicker.css"); ?>" type="text/css"</style>
		