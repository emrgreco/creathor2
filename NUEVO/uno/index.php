
		<?php
		require_once ("../config/db.php");
		require_once ("../config/conexion.php");
				$activeuno="";
				$activeuno="active";
				$activeuno="";
		$title="uno | OBRAS";
		?>


		<!DOCTYPE html>
		<html lang="es">
		  <head>
			<?php include("../head.php");?>
		  </head>
		  <body>
		 	<?php include("../navbar1.php");?> 
		    <div class="container">
		    	<br>
				<div class="panel panel-default">
					<div class="panel-heading">
						<div class="row">
						</div>
					</div>			
					<div class="panel-body">
					<?php
						include("../modal/uno_add.php");
						include("../modal/uno_edit.php");
					?>
					<div class="row">
						<form class="form-horizontal col-md-11" role="form" id="datos_preest">
							<div class="form-group row">
								<div class="col-md-10">
									<input type="text" class="form-control" id="q" placeholder="Nombre" onkeyup="load(1);">
								</div>
								<div class="col-md-2">
									<button type="button" class="btn btn-primary" onclick="loaduno(1);"><span class="fas fa-xs fa-search" ></span> Buscar</button>
								</div>
							</div>
						</form>
					    <div class="col-md-1">
							<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#uno_modal_add"><span class="fas fa-xs fa-plus" ></span></button>
						</div>
					</div>
					<span id="loader"></span>
					<div id="resultadosuno"></div>
					<div class="outer_divuno"></div>
					</div>
				</div>
			</div>
			<hr>
			<?php
				include("../footer.php");
			?>
			<script type="text/javascript" src="../js/uno.js"></script>
		  </body>
		</html>

		