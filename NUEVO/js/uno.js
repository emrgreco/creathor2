		
		$(document).ready(function(){
			$("#resultadosuno").fadeOut(2000);
			loaduno(1);
		});

		function getParameterByName(name)
		{
		    if (name !== "" && name !== null && name != undefined)
		    {
		        name = name.replace(/[\[]/, "\[").replace(/[\]]/, "\]");
		        var regex = new RegExp("[\?&]" + name + "=([^&#]*)"),
		            results = regex.exec(location.search);
		        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
		    }
		    else
		    {
		        var arr = location.href.split("/");
		        return arr[arr.length - 1];
		    }
		}
		
		function loaduno(page)
		{
			var CONCEPTOS = getParameterByName('CONCEPTOS');
			var q= $("#q").val();
			$("#loader").fadeIn("slow");
			$.ajax({
				
					url:"../actions/uno_search.php?action=ajax&CONCEPTOS="+CONCEPTOS+"&page="+page+"&q="+q,
					beforeSend: function(objeto)
				{
					$("#loader").html("<img src='../img/ajax-loader.gif'> Cargando...");
				},
				success:function(data)
				{
					$(".outer_divuno").html(data).fadeIn("slow");
					$("#loader").html("");
					borrar_modalesuno();
					$("#uno_modal_add").removeData();

					//$("#uno_modal_add" ).modal("hide");
					//$("#uno_modal_edit" ).modal("hide");
				}
			});
		}
		
		function eliminaruno (id)
		{
			var q= $("#q").val();
			if (confirm("Realmente deseas eliminar el uno"))
			{	
				$.ajax({
					type: "GET",
					url: "../actions/uno_search.php",
					data: "id="+id,"q":q,
					beforeSend: function(objeto)
					{
						$("#resultadosuno").html("Mensaje: Cargando...");
					},
					success: function(datos)
					{
						$("#resultadosuno").html(datos);
						loaduno(1);
					}
				});
			}
		}
		
		$("#uno_add_form" ).submit(function( event ) {
			$("#uno_guardar_datos").attr("disabled", true);
				var formData = new FormData();
	    		
					formData.append("add_uno_dos_campo", add_uno_dos_campo);
					formData.append("add_uno_OTRAIMAGEN_campo", add_uno_OTRAIMAGEN_campo);
					formData.append("add_uno_MASIMAGEN_campo", add_uno_MASIMAGEN_campo);
				var objArr = [];
				
								objArr.push("add_uno_tres_campo", add_uno_tres_campo );var_add_OTRO=document.getElementById("add_uno_OTRO_campo");
								objArr.push("add_uno_OTRO_campo", var_add_OTRO);

				formData.append("objArr", JSON.stringify( objArr ));
			 $.ajax({
					type: "POST",
					url: "../actions/uno_add_action.php",
					data: formData,
					beforeSend: function(objeto)
					{
						$("#uno_resultados_modal_add").html("Mensaje: Cargando...");
					},
					success: function(datos)
					{
						$("#uno_resultados_modal_add").html(datos);
						$("#uno_guardar_datos").attr("disabled", false);
						loaduno(1);
				  	}
			});
			event.preventDefault();
		});

		$("#uno_edit_form" ).submit(function( event ) {
			$("#uno_actualizar_datos").attr("disabled", true);
				var formData = new FormData();
	    		
					formData.append("edit_uno_dos_campo", edit_uno_dos_campo);
					formData.append("edit_uno_OTRAIMAGEN_campo", edit_uno_OTRAIMAGEN_campo);
					formData.append("edit_uno_MASIMAGEN_campo", edit_uno_MASIMAGEN_campo);
				var objArr = [];
				
								objArr.push("edit_uno_tres_campo", edit_uno_tres_campo);var_edit_OTRO=document.getElementById("edit_uno_OTRO_campo");
								objArr.push("edit_uno_OTRO_campo", var_edit_OTRO);
				formData.append("objArr", JSON.stringify( objArr ));
			$.ajax({
				type: "POST",
				url: "../actions/uno_edit_action.php",
				data: formData,
				beforeSend: function(objeto)
				{
					$("#uno_resultados_modal_edit").html("Mensaje: Cargando...");
				},
				success: function(datos)
				{
					$("#uno_resultados_modal_edit").html(datos);
					loaduno(1);
				}
			});
			event.preventDefault();
		});

		function obtener_datosuno(id)
		{
		
						$("#edit_uno_id_indice").val("");
						$("#edit_uno_id_indice").val(id);

			var CONCEPTOS = getParameterByName('CONCEPTOS');
				$("#edit_uno_ID_CONCEPTOS_externo").val("");
				$("#edit_uno_ID_CONCEPTOS_externo").val(CONCEPTOS);

				$("#edit_img_uno_dos_campo").attr("src",$("#img_dos"+id).attr("src"));
				//unodos();
					$("#edit_uno_tres_campo").val("");
					var tres = $("#edit_uno_tres"+id).val();
					$("#edit_uno_tres_campo").val(tres);

				$("#edit_img_uno_OTRAIMAGEN_campo").attr("src",$("#img_OTRAIMAGEN"+id).attr("src"));
				//unoOTRAIMAGEN();
					$("#edit_uno_OTRO_campo").val("");
					var OTRO = $("#edit_uno_OTRO"+id).val();
					$("#edit_uno_OTRO_campo").val(OTRO);

				$("#edit_img_uno_MASIMAGEN_campo").attr("src",$("#img_MASIMAGEN"+id).attr("src"));
				//unoMASIMAGEN();
		}

		function borrar_modalesuno()
		{
			$("#edit_uno_dos").val("");
			$("#add_uno_dos").val("");
			$("#edit_uno_tres").val("");
			$("#add_uno_tres").val("");
			$("#edit_uno_OTRAIMAGEN").val("");
			$("#add_uno_OTRAIMAGEN").val("");
			$("#edit_uno_OTRO").val("");
			$("#add_uno_OTRO").val("");
			$("#edit_uno_MASIMAGEN").val("");
			$("#add_uno_MASIMAGEN").val("");
			$("#edit_uno_id").val("");
			$("#add_uno_id").val("");
				$("#edit_uno_ID_uno").val("");
				$("#add_uno_ID_uno").val("");
				
			}