		
		$(document).ready(function(){
			$("#resultadosCONCEPTOS").fadeOut(2000);
			loadCONCEPTOS(1);
		});

		function getParameterByName(name)
		{
		    if (name !== "" && name !== null && name != undefined)
		    {
		        name = name.replace(/[\[]/, "\[").replace(/[\]]/, "\]");
		        var regex = new RegExp("[\?&]" + name + "=([^&#]*)"),
		            results = regex.exec(location.search);
		        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
		    }
		    else
		    {
		        var arr = location.href.split("/");
		        return arr[arr.length - 1];
		    }
		}
		
		function loadCONCEPTOS(page)
		{
			var q= $("#q").val();
			$("#loader").fadeIn("slow");
			$.ajax({
				
					url:"../actions/CONCEPTOS_search.php?action=ajax&page="+page+"&q="+q,
					beforeSend: function(objeto)
				{
					$("#loader").html("<img src='../img/ajax-loader.gif'> Cargando...");
				},
				success:function(data)
				{
					$(".outer_divCONCEPTOS").html(data).fadeIn("slow");
					$("#loader").html("");
					borrar_modalesCONCEPTOS();
					$("#CONCEPTOS_modal_add").removeData();

					//$("#CONCEPTOS_modal_add" ).modal("hide");
					//$("#CONCEPTOS_modal_edit" ).modal("hide");
				}
			});
		}
		
		function eliminarCONCEPTOS (id)
		{
			var q= $("#q").val();
			if (confirm("Realmente deseas eliminar el CONCEPTOS"))
			{	
				$.ajax({
					type: "GET",
					url: "../actions/CONCEPTOS_search.php",
					data: "id="+id,"q":q,
					beforeSend: function(objeto)
					{
						$("#resultadosCONCEPTOS").html("Mensaje: Cargando...");
					},
					success: function(datos)
					{
						$("#resultadosCONCEPTOS").html(datos);
						loadCONCEPTOS(1);
					}
				});
			}
		}
		
		$("#CONCEPTOS_add_form" ).submit(function( event ) {
			$("#CONCEPTOS_guardar_datos").attr("disabled", true);
				var formData = new FormData();
	    		
					formData.append("add_CONCEPTOS_PRUEBA_campo", add_CONCEPTOS_PRUEBA_campo);
				var objArr = [];
				
								objArr.push("add_CONCEPTOS_CLAVE_campo", add_CONCEPTOS_CLAVE_campo );
								objArr.push("add_CONCEPTOS_PRECIO_campo", add_CONCEPTOS_PRECIO_campo );
								objArr.push("add_CONCEPTOS_UNIDAD_campo", add_CONCEPTOS_UNIDAD_campo );
								objArr.push("add_CONCEPTOS_ARCHIVOS_campo", add_CONCEPTOS_ARCHIVOS_campo );

				formData.append("objArr", JSON.stringify( objArr ));
			 $.ajax({
					type: "POST",
					url: "../actions/CONCEPTOS_add_action.php",
					data: formData,
					beforeSend: function(objeto)
					{
						$("#CONCEPTOS_resultados_modal_add").html("Mensaje: Cargando...");
					},
					success: function(datos)
					{
						$("#CONCEPTOS_resultados_modal_add").html(datos);
						$("#CONCEPTOS_guardar_datos").attr("disabled", false);
						loadCONCEPTOS(1);
				  	}
			});
			event.preventDefault();
		});

		$("#CONCEPTOS_edit_form" ).submit(function( event ) {
			$("#CONCEPTOS_actualizar_datos").attr("disabled", true);
				var formData = new FormData();
	    		
					formData.append("edit_CONCEPTOS_PRUEBA_campo", edit_CONCEPTOS_PRUEBA_campo);
				var objArr = [];
				
								objArr.push("edit_CONCEPTOS_CLAVE_campo", edit_CONCEPTOS_CLAVE_campo);
								objArr.push("edit_CONCEPTOS_PRECIO_campo", edit_CONCEPTOS_PRECIO_campo);
								objArr.push("edit_CONCEPTOS_UNIDAD_campo", edit_CONCEPTOS_UNIDAD_campo);
								objArr.push("edit_CONCEPTOS_ARCHIVOS_campo", edit_CONCEPTOS_ARCHIVOS_campo);
				formData.append("objArr", JSON.stringify( objArr ));
			$.ajax({
				type: "POST",
				url: "../actions/CONCEPTOS_edit_action.php",
				data: formData,
				beforeSend: function(objeto)
				{
					$("#CONCEPTOS_resultados_modal_edit").html("Mensaje: Cargando...");
				},
				success: function(datos)
				{
					$("#CONCEPTOS_resultados_modal_edit").html(datos);
					loadCONCEPTOS(1);
				}
			});
			event.preventDefault();
		});

		function obtener_datosCONCEPTOS(id)
		{
		
						$("#edit_CONCEPTOS_ID_indice").val("");
						$("#edit_CONCEPTOS_ID_indice").val(id);

					$("#edit_CONCEPTOS_CLAVE_campo").val("");
					var CLAVE = $("#edit_CONCEPTOS_CLAVE"+id).val();
					$("#edit_CONCEPTOS_CLAVE_campo").val(CLAVE);

					$("#edit_CONCEPTOS_PRECIO_campo").val("");
					var PRECIO = $("#edit_CONCEPTOS_PRECIO"+id).val();
					$("#edit_CONCEPTOS_PRECIO_campo").val(PRECIO);

					$("#edit_CONCEPTOS_UNIDAD_campo").val("");
					var UNIDAD = $("#edit_CONCEPTOS_UNIDAD"+id).val();
					$("#edit_CONCEPTOS_UNIDAD_campo").val(UNIDAD);

					$("#edit_CONCEPTOS_ARCHIVOS_campo").val("");
					var ARCHIVOS = $("#edit_CONCEPTOS_ARCHIVOS"+id).val();
					$("#edit_CONCEPTOS_ARCHIVOS_campo").val(ARCHIVOS);

				$("#edit_img_CONCEPTOS_PRUEBA_campo").attr("src",$("#img_PRUEBA"+id).attr("src"));
				//unoPRUEBA();
		}

		function borrar_modalesCONCEPTOS()
		{
			$("#edit_CONCEPTOS_CLAVE").val("");
			$("#add_CONCEPTOS_CLAVE").val("");
			$("#edit_CONCEPTOS_PRECIO").val("");
			$("#add_CONCEPTOS_PRECIO").val("");
			$("#edit_CONCEPTOS_UNIDAD").val("");
			$("#add_CONCEPTOS_UNIDAD").val("");
			$("#edit_CONCEPTOS_ARCHIVOS").val("");
			$("#add_CONCEPTOS_ARCHIVOS").val("");
			$("#edit_CONCEPTOS_PRUEBA").val("");
			$("#add_CONCEPTOS_PRUEBA").val("");
			$("#edit_CONCEPTOS_ID").val("");
			$("#add_CONCEPTOS_ID").val("");
			}