		
		$(document).ready(function(){
			$("#resultadosoais").fadeOut(2000);
			loadoais(1);
		});

		function getParameterByName(name)
		{
		    if (name !== "" && name !== null && name != undefined)
		    {
		        name = name.replace(/[\[]/, "\[").replace(/[\]]/, "\]");
		        var regex = new RegExp("[\?&]" + name + "=([^&#]*)"),
		            results = regex.exec(location.search);
		        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
		    }
		    else
		    {
		        var arr = location.href.split("/");
		        return arr[arr.length - 1];
		    }
		}
		
		function loadoais(page)
		{
			var CONCEPTOS = getParameterByName('CONCEPTOS');
			var q= $("#q").val();
			$("#loader").fadeIn("slow");
			$.ajax({
				
					url:"../actions/oais_search.php?action=ajax&CONCEPTOS="+CONCEPTOS+"&page="+page+"&q="+q,
					beforeSend: function(objeto)
				{
					$("#loader").html("<img src='../img/ajax-loader.gif'> Cargando...");
				},
				success:function(data)
				{
					$(".outer_divoais").html(data).fadeIn("slow");
					$("#loader").html("");
					borrar_modalesoais();
					$("#oais_modal_add").removeData();

					//$("#oais_modal_add" ).modal("hide");
					//$("#oais_modal_edit" ).modal("hide");
				}
			});
		}
		
		function eliminaroais (id)
		{
			var q= $("#q").val();
			if (confirm("Realmente deseas eliminar el oais"))
			{	
				$.ajax({
					type: "GET",
					url: "../actions/oais_search.php",
					data: "id="+id,"q":q,
					beforeSend: function(objeto)
					{
						$("#resultadosoais").html("Mensaje: Cargando...");
					},
					success: function(datos)
					{
						$("#resultadosoais").html(datos);
						loadoais(1);
					}
				});
			}
		}
		
		$("#oais_add_form" ).submit(function( event ) {
			$("#oais_guardar_datos").attr("disabled", true);
		 		var formData = $(this).serialize();
		 	
			 $.ajax({
					type: "POST",
					url: "../actions/oais_add_action.php",
					data: formData,
					beforeSend: function(objeto)
					{
						$("#oais_resultados_modal_add").html("Mensaje: Cargando...");
					},
					success: function(datos)
					{
						$("#oais_resultados_modal_add").html(datos);
						$("#oais_guardar_datos").attr("disabled", false);
						loadoais(1);
				  	}
			});
			event.preventDefault();
		});

		$("#oais_edit_form" ).submit(function( event ) {
			$("#oais_actualizar_datos").attr("disabled", true);
		 		var formData = $(this).serialize();
		 	
			$.ajax({
				type: "POST",
				url: "../actions/oais_edit_action.php",
				data: formData,
				beforeSend: function(objeto)
				{
					$("#oais_resultados_modal_edit").html("Mensaje: Cargando...");
				},
				success: function(datos)
				{
					$("#oais_resultados_modal_edit").html(datos);
					loadoais(1);
				}
			});
			event.preventDefault();
		});

		function obtener_datosoais(id)
		{
		
						$("#edit_oais_id_indice").val("");
						$("#edit_oais_id_indice").val(id);

			var CONCEPTOS = getParameterByName('CONCEPTOS');
				$("#edit_oais_ID_CONCEPTOS_externo").val("");
				$("#edit_oais_ID_CONCEPTOS_externo").val(CONCEPTOS);

					$("#edit_oais_dos_campo").val("");
					var dos = $("#edit_oais_dos"+id).val();
					$("#edit_oais_dos_campo").val(dos);

		}

		function borrar_modalesoais()
		{
			$("#edit_oais_dos").val("");
			$("#add_oais_dos").val("");
			$("#edit_oais_id").val("");
			$("#add_oais_id").val("");
				$("#edit_oais_ID_oais").val("");
				$("#add_oais_ID_oais").val("");
				
			}