
				DROP DATABASE IF EXISTS OBRAS;
				CREATE DATABASE IF NOT EXISTS OBRAS;
				SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
				SET time_zone = "+00:00"; 

				CREATE TABLE CONCEPTOS (`CLAVE` VARCHAR(10) NOT NULL, `PRECIO` DOUBLE , `UNIDAD` VARCHAR(10), `ARCHIVOS` VARCHAR(20), `PRUEBA` VARCHAR(30), `ID` INT(10) NOT NULL) ENGINE=InnoDB DEFAULT CHARSET=utf8;
						ALTER TABLE `CONCEPTOS` ADD PRIMARY KEY (`ID`), ADD KEY `ID` (`ID`);
						ALTER TABLE `CONCEPTOS` MODIFY `ID` int(10)  NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=0;

				CREATE TABLE uno (`dos` VARCHAR(30), `tres` VARCHAR(10), `OTRAIMAGEN` VARCHAR(50), `OTRO` TINYINT(1), `MASIMAGEN` VARCHAR(100), `id` INT(10), `uno_ID_CONCEPTOS` INT(10) NOT NULL) ENGINE=InnoDB DEFAULT CHARSET=utf8;
						ALTER TABLE `uno` ADD PRIMARY KEY (`id`), ADD KEY `id` (`id`);
						ALTER TABLE `uno` MODIFY `id` int(10)  NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=0;

				CREATE TABLE oais (`dos` VARCHAR(30) NOT NULL, `id` INT(10), `oais_ID_CONCEPTOS` INT(10) NOT NULL) ENGINE=InnoDB DEFAULT CHARSET=utf8;
						ALTER TABLE `oais` ADD PRIMARY KEY (`id`), ADD KEY `id` (`id`);
						ALTER TABLE `oais` MODIFY `id` int(10)  NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=0;

				CREATE TABLE `lugares` (
				  `id` int(11) NOT NULL,
				  `id_session` int(11) NOT NULL,
				  `ip` varchar(20) NOT NULL,
				  `fecha` datetime NOT NULL
				) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

				CREATE TABLE `usuarios` (
				  `id` int(11) NOT NULL,
				  `password` varchar(130) NOT NULL,
				  `nombre` varchar(30) NOT NULL,
				  `appater` varchar(30) NOT NULL,
				  `apmater` varchar(30) NOT NULL,
				  `correo` varchar(80) NOT NULL,
				  `activacion` int(11) NOT NULL DEFAULT 0,
				  `token` varchar(40) NOT NULL,
				  `token_password` varchar(100) DEFAULT NULL,
				  `password_request` int(11) DEFAULT 0,
				  `tokensesion` varchar(100) NOT NULL
				) ENGINE=InnoDB DEFAULT CHARSET=utf8;

				INSERT INTO `usuarios` (`id`, `password`, `nombre`, `appater`, `apmater`, `correo`, `activacion`, `token`, `token_password`, `password_request`, `tokensesion`) VALUES
				(0, '$2y$10/raypuqF79NVVfN//oPQ60BdCMhMu5O', 'Juan', 'Perez', 'Cid', 'admin@admin.com', 1, '1b0b63d725e598e48cc88ebe036b172c', '', 0, '');
				ALTER TABLE `lugares`
				  ADD PRIMARY KEY (`id`);
				ALTER TABLE `usuarios`
				  ADD PRIMARY KEY (`id`);
				ALTER TABLE `lugares`
				  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=0;
				ALTER TABLE `usuarios`
				  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;