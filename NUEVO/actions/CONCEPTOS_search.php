<?php 

		require_once ("../config/db.php");
		require_once ("../config/conexion.php");
		$action = (isset($_REQUEST["action"])&& $_REQUEST["action"] !=NULL)?$_REQUEST["action"]:"";
		if (isset($_GET["id"]))
		{
			$ID=intval($_GET["id"]);
			$query=mysqli_query($con, "select * from CONCEPTOS WHERE ID = $ID ");
			$rw_user=mysqli_fetch_array($query);
			$count=$rw_user["ID"];


			$query=mysqli_query($con, "SELECT COUNT(*) as total FROM CONCEPTOS");
			$rw_user=mysqli_fetch_array($query);
			
			if ($rw_user["total"]>1)
			{
				$consulta_file=mysqli_query($con,"SELECT * FROM CONCEPTOS WHERE ID=$ID;");
				$file=mysqli_fetch_array($consulta_file);
				unlink( "../CONCEPTOS/HOST/".$file["PRUEBA"]);
				if ($delete1=mysqli_query($con,"DELETE FROM CONCEPTOS WHERE ID=$ID LIMIT 1;"))
				{
				?>
				<div class="alert alert-success alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <strong>Aviso!</strong> Datos eliminados exitosamente.
				</div>
				<?php 
				}else {
					?>
					<div class="alert alert-danger alert-dismissible" role="alert">
					  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					  <strong>Error!</strong> Lo siento algo ha salido mal intenta nuevamente.
					</div>
					<?php
					
				}
				
			} else {
				?>
				<div class="alert alert-danger alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <strong>Error!</strong> No se puede eliminar todos los usuarios. 
				</div>
				<?php
			}
		}
		if($action == "ajax")
		{
		     $q = mysqli_real_escape_string($con,(strip_tags($_REQUEST["q"], ENT_QUOTES)));
			 $aColumns = array('UNIDAD');
			 $sTable = "CONCEPTOS";
			 $sWhere = "";
			if ( $_GET["q"] != "" )
			{
				$sWhere = "WHERE (";
				for ( $i=0 ; $i<count($aColumns) ; $i++ )
				{
					$sWhere .= $aColumns[$i]." LIKE '%$q%' OR ";
				}
				$sWhere = substr_replace( $sWhere, "", -3 );
				$sWhere .= ")";
			}
			$sWhere.=" order by ID desc";
			include "pagination.php";
			$page = (isset($_REQUEST["page"]) && !empty($_REQUEST["page"]))?$_REQUEST["page"]:1;
			$per_page = 10;
			$adjacents  = 4;
			$offset = ($page - 1) * $per_page;
			$count_query   = mysqli_query($con, "SELECT count(*) AS numrows FROM $sTable  $sWhere");
			$row= mysqli_fetch_array($count_query);
			$numrows = $row["numrows"];
			$total_pages = ceil($numrows/$per_page);
			$reload = "../CONCEPTOS/index.php";
			$sql="SELECT * FROM  $sTable $sWhere LIMIT $offset, $per_page";
			//echo $sql;
			$query = mysqli_query($con, $sql);
			if ($numrows>0)
			{
				
				?>
				<div class="table-responsive">
				  <table class="table table-hover">
					<tr  class="default">

				
<th>CLAVE</th>
<th>PRECIO</th>
<th>UNIDAD</th>
<th>ARCHIVOS</th>
<th>PRUEBA</th>

						<th><span class="pull-right">Acciones</span></th>
						
					</tr>
					<?php
					while ($row=mysqli_fetch_array($query))
					{
					
$ID=$row["ID"];
$CLAVE=$row["CLAVE"];
$PRECIO=$row["PRECIO"];
$UNIDAD=$row["UNIDAD"];
$ARCHIVOS=$row["ARCHIVOS"];
$PRUEBA=$row["PRUEBA"];
	
						?>
<input type="hidden" value="<?php echo $row["ID"];?>" id="edit_CONCEPTOS_ID<?php echo $ID;?>">
<input type="hidden" value="<?php echo $row["CLAVE"];?>" id="edit_CONCEPTOS_CLAVE<?php echo $ID;?>">
<input type="hidden" value="<?php echo $row["PRECIO"];?>" id="edit_CONCEPTOS_PRECIO<?php echo $ID;?>">
<input type="hidden" value="<?php echo $row["UNIDAD"];?>" id="edit_CONCEPTOS_UNIDAD<?php echo $ID;?>">
<input type="hidden" value="<?php echo $row["ARCHIVOS"];?>" id="edit_CONCEPTOS_ARCHIVOS<?php echo $ID;?>">
<input type="hidden" value="<?php echo $row["PRUEBA"];?>" id="edit_CONCEPTOS_PRUEBA<?php echo $ID;?>">
	
						<tr>
<td><?php echo $CLAVE; ?></td>
<td><?php echo $PRECIO; ?></td>
<td><?php echo $UNIDAD; ?></td>
<td><?php echo $ARCHIVOS; ?></td>
<td><img id="img_PRUEBA<?php echo $ID;?>" src="../CONCEPTOS/HOST/<?php echo $PRUEBA; ?>" width="100"></td>

						<td >
							<span class="pull-right">

							
									<a href="ver.php?CONCEPTOS=<?php echo $ID;?>" class="btn btn-secondary" title="Ver CONCEPTOS" >
									<i class="fas fa-xs fa-eye"></i>
									</a> 
								<a href="#" class="btn btn-secondary" title="Editar CONCEPTOS" onclick="obtener_datosCONCEPTOS('<?php echo $ID;?>');" data-toggle="modal" data-target="#CONCEPTOS_modal_edit">
								<i class="fas fa-xs fa-edit"></i>
								</a> 

								<a href="#" class="btn btn-secondary" title="Borrar CONCEPTOS" onclick="eliminarCONCEPTOS('<?php echo $ID; ?>');">
								<i class="fas fa-xs fa-trash"></i>
								</a>
							</span>
						</td>
							
						</tr>
						<?php
					}
					?>
					<tr>
						<td colspan=9>
							<span class="pull-right">
								<?php echo paginate($reload, $page, $total_pages, $adjacents); ?>	
							</span>
						</td>
					</tr>
				  </table>
				</div>
				<?php
			}
		}?>
		
