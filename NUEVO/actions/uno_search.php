<?php 

		require_once ("../config/db.php");
		require_once ("../config/conexion.php");
		$action = (isset($_REQUEST["action"])&& $_REQUEST["action"] !=NULL)?$_REQUEST["action"]:"";
		if (isset($_GET["id"]))
		{
			$id=intval($_GET["id"]);
			$query=mysqli_query($con, "select * from uno WHERE id = $id ");
			$rw_user=mysqli_fetch_array($query);
			$count=$rw_user["id"];


			$query=mysqli_query($con, "SELECT COUNT(*) as total FROM uno");
			$rw_user=mysqli_fetch_array($query);
			
			if ($rw_user["total"]>=1)
			{
				$consulta_file=mysqli_query($con,"SELECT * FROM uno WHERE id=$id;");
				$file=mysqli_fetch_array($consulta_file);
				unlink( "../uno/uploads/".$file["dos"]);
				unlink( "../uno/wakandas/".$file["OTRAIMAGEN"]);
				unlink( "../uno/OTRAMAS/".$file["MASIMAGEN"]);
				if ($delete1=mysqli_query($con,"DELETE FROM uno WHERE id=$id LIMIT 1;"))
				{
				?>
				<div class="alert alert-success alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <strong>Aviso!</strong> Datos eliminados exitosamente.
				</div>
				<?php 
				}else {
					?>
					<div class="alert alert-danger alert-dismissible" role="alert">
					  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					  <strong>Error!</strong> Lo siento algo ha salido mal intenta nuevamente.
					</div>
					<?php
					
				}
				
			} else {
				?>
				<div class="alert alert-danger alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <strong>Error!</strong> No se puede eliminar todos los usuarios. 
				</div>
				<?php
			}
		}
		if($action == "ajax")
		{
		     $q = mysqli_real_escape_string($con,(strip_tags($_REQUEST["q"], ENT_QUOTES)));
			 $aColumns = array();
			 $sTable = "uno";
				 $sWhere = "WHERE uno_ID_CONCEPTOS=".$_GET['CONCEPTOS'];
				if ( $_GET["q"] != "" )
				{
					$sWhere = " AND (";
					for ( $i=0 ; $i<count($aColumns) ; $i++ )
					{
						$sWhere .= $aColumns[$i]." LIKE '%$q%' OR ";
					}
					$sWhere = substr_replace( $sWhere, "", -3 );
					$sWhere .= ")";
				}
			$sWhere.=" order by id desc";
			include "pagination.php";
			$page = (isset($_REQUEST["page"]) && !empty($_REQUEST["page"]))?$_REQUEST["page"]:1;
			$per_page = 10;
			$adjacents  = 4;
			$offset = ($page - 1) * $per_page;
			$count_query   = mysqli_query($con, "SELECT count(*) AS numrows FROM $sTable  $sWhere");
			$row= mysqli_fetch_array($count_query);
			$numrows = $row["numrows"];
			$total_pages = ceil($numrows/$per_page);
			$reload = "../uno/index.php";
			$sql="SELECT * FROM  $sTable $sWhere LIMIT $offset, $per_page";
			//echo $sql;
			$query = mysqli_query($con, $sql);
			if ($numrows>0)
			{
				
				?>
				<div class="table-responsive">
				  <table class="table table-hover">
					<tr  class="default">

				
<th>dos</th>
<th>tres</th>
<th>OTRAIMAGEN</th>
<th>OTRO</th>
<th>MASIMAGEN</th>

						<th><span class="pull-right">Acciones</span></th>
						
					</tr>
					<?php
					while ($row=mysqli_fetch_array($query))
					{
					
$id=$row["id"];
$uno_ID_CONCEPTOS=$row["uno_ID_CONCEPTOS"];
$dos=$row["dos"];
$tres=$row["tres"];
$OTRAIMAGEN=$row["OTRAIMAGEN"];
$OTRO=$row["OTRO"];
$MASIMAGEN=$row["MASIMAGEN"];
	
						?>
<input type="hidden" value="<?php echo $row["id"];?>" id="edit_uno_id<?php echo $id;?>">
<input type="hidden" value="<?php echo $row["dos"];?>" id="edit_uno_dos<?php echo $id;?>">
<input type="hidden" value="<?php echo $row["tres"];?>" id="edit_uno_tres<?php echo $id;?>">
<input type="hidden" value="<?php echo $row["OTRAIMAGEN"];?>" id="edit_uno_OTRAIMAGEN<?php echo $id;?>">
<input type="hidden" value="<?php echo $row["OTRO"];?>" id="edit_uno_OTRO<?php echo $id;?>">
<input type="hidden" value="<?php echo $row["MASIMAGEN"];?>" id="edit_uno_MASIMAGEN<?php echo $id;?>">
	
						<tr>
<td><img id="img_dos<?php echo $id;?>" src="../uno/uploads/<?php echo $dos; ?>" width="100"></td>
<td><?php echo $tres; ?></td>
<td><img id="img_OTRAIMAGEN<?php echo $id;?>" src="../uno/wakandas/<?php echo $OTRAIMAGEN; ?>" width="100"></td>
<td><?php echo $OTRO; ?></td>
<td><img id="img_MASIMAGEN<?php echo $id;?>" src="../uno/OTRAMAS/<?php echo $MASIMAGEN; ?>" width="100"></td>

						<td >
							<span class="pull-right">

							
								<a href="#" class="btn btn-secondary" title="Editar uno" onclick="obtener_datosuno('<?php echo $id;?>');" data-toggle="modal" data-target="#uno_modal_edit">
								<i class="fas fa-xs fa-edit"></i>
								</a> 

								<a href="#" class="btn btn-secondary" title="Borrar uno" onclick="eliminaruno('<?php echo $id; ?>');">
								<i class="fas fa-xs fa-trash"></i>
								</a>
							</span>
						</td>
							
						</tr>
						<?php
					}
					?>
					<tr>
						<td colspan=9>
							<span class="pull-right">
								<?php echo paginate($reload, $page, $total_pages, $adjacents); ?>	
							</span>
						</td>
					</tr>
				  </table>
				</div>
				<?php
			}
		}?>
		
