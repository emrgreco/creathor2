<?php 


require_once './config/db.php';
require_once './config/conexion.php';

global $con;


class tabla extends sitio
{
	public $id_tabla;
	public $clave_tabla;
	public $tipo_tabla;
	public $desplegable;
	public $id_icono;
	public $id_archivo;
	public $nombre_origen;
	public $nombre_destino;


	public $indice;
	public $visible;
	public $editable;
	public $requerido;
	public $busqueda;
	public $campos;
	public $archivos;
	public $indices_externos;
	public $subtablas;

	public function __construct($datos)
	{
		$sql ="SELECT * FROM campos WHERE id_tabla=".$datos['id_tabla'];
		$sql_result=mysqli_query($GLOBALS['con'], $sql);

		$sql_count="SELECT COUNT(*) as count FROM campos WHERE id_tabla=".$datos['id_tabla'];
		$sql_count2=mysqli_fetch_assoc(mysqli_query($GLOBALS['con'], $sql_count));

		$this->id_tabla=$datos['id_tabla'];
		$this->desplegable = $datos['desplegable'];
		$this->clave_tabla = $datos['clave_tabla'];
		$this->tipo_tabla=$datos['tipo_tabla'];
		$this->id_icono=$datos['icono'];
		$this->nombre_origen=$datos['tabla_principal'];
		$this->nombre_destino=$datos['tabla_liga'];
		if(intval($sql_count2['count'])>0)
		{
			while($row_result=mysqli_fetch_assoc($sql_result))
			{

				if ($row_result['indice']==1)
				{
					$this->indice[]=$row_result;
				}
				else if ($row_result['visible']==1)
				{
					$this->visible[]=$row_result;
					$this->campos[]=$row_result;
				}
				else if ($row_result['editable']==1)
				{
					$this->editable[]=$row_result;
					$this->campos[]=$row_result;
				}
				else if ($row_result['requerido']==1)
				{
					$this->requerido[]=$row_result;
					$this->campos[]=$row_result;
				}
				else if ($row_result['busqueda']==1)
				{
					$this->busqueda[]=$row_result;
					$this->campos[]=$row_result;
				}
				
				if ($row_result['tipo_campo']==6)
				{
					$this->id_archivo=1;
					$this->archivos[]=$row_result;
				}
			}

			if($this->tipo_tabla==1)//Si es una tabla principal, busca toda
			{
				$sql ="SELECT * FROM tabla WHERE tipo_tabla=".$this->id_tabla;
				$sql_result=mysqli_query($GLOBALS['con'], $sql);
				while($row_result=mysqli_fetch_assoc($sql_result))
				{
					$this->subtablas[]=$row_result;
					$sql1 ="SELECT * FROM campos WHERE id_tabla=".$row_result['id_tabla'];
					$sql1_result=mysqli_query($GLOBALS['con'], $sql1);
					while($row1_result=mysqli_fetch_assoc($sql1_result))
					{
					}	
				}
			}
			else if($this->tipo_tabla==2)//si es subtabla buscar el indice 
			{
				$sql ="SELECT * FROM tabla WHERE id_tabla=".$this->nombre_origen;
				$sql_result=mysqli_query($GLOBALS['con'], $sql);
				while($row_result=mysqli_fetch_assoc($sql_result))
				{

					$this->subtablas[]=$row_result;
					$this->nombre_origen=$row_result['clave_tabla'];
					$sql1 ="SELECT * FROM campos WHERE id_tabla=".$row_result['id_tabla'];
					$sql1_result=mysqli_query($GLOBALS['con'], $sql1);
					while($row1_result=mysqli_fetch_assoc($sql1_result))
					{
						if($row1_result['indice']==1)
						{
							$this->indices_externos[]=$row1_result;
						}
					}
				}
			}
		}
		else if($this->tipo_tabla==3)
		{
			$sql ="SELECT * FROM campos WHERE id_tabla=".$this->nombre_origen;
			$sql_result=mysqli_query($GLOBALS['con'], $sql);
			while($row_result=mysqli_fetch_assoc($sql_result))
			{

			}
		}
	}

	public function info()
	{
		echo "<h1>Tabla: ".$this->clave_tabla."</h1>";
		echo "<h2>Identificador: ".$this->id_tabla."</h2>";
		echo "<h2>Archivos: ";
		echo ($this->id_archivo==1)?'Si':'NO'."</h2>";
		echo ($this->desplegable==1)?("<h2>tipo tabla: Pagina</h2>"):("<h2>tipo tabla: Modal</h2>");
		if($this->tipo_tabla==1)
		{
			echo "<h1>Tabla principal "."</h1>";

			foreach ($this->subtablas as $tabla )
			{
				echo "<h2>+".$tabla['clave_tabla']."</h2>";
			}

			$tabla_row='
				<table class="table table-sm table-hover table-stripe">
					<thead>
						<tr>
						<th>Clave</th>
						<th>Tipo</th>
						<th>Indice</th>
						<th>Visible</th>
						<th>Editable</th>
						<th>Requerido</th>
						<th>Busqueda</th>
						</tr>
					</thead>
					<tbody>';
			
			foreach ($this->indice as $campo)
			{
				$tabla_row.='<tr>'
							.'<td>'.$campo['clave_campo'].'</td>'
							.'<td>'.$this->tipo_dato($campo['tipo_campo']).'</td>'
							.'<td>'.$campo['indice'].'</td>'
							.'<td>'.$campo['visible'].'</td>'
							.'<td>'.$campo['editable'].'</td>'
							.'<td>'.$campo['requerido'].'</td>'
							.'<td>'.$campo['busqueda'].'</td>'
						.'</tr>';

			}
			foreach ($this->campos as $campo)
			{
				$tabla_row.='<tr>'
							.'<td>'.$campo['clave_campo'].'</td>'
							.'<td>'.$this->tipo_dato($campo['tipo_campo']).'</td>'
							.'<td>'.$campo['indice'].'</td>'
							.'<td>'.$campo['visible'].'</td>'
							.'<td>'.$campo['editable'].'</td>'
							.'<td>'.$campo['requerido'].'</td>'
							.'<td>'.$campo['busqueda'].'</td>'
						.'</tr>';
			}
			if(isset($this->indices_externos))
			{
				foreach ($this->indices_externos as $campo)
				{
					$tabla_row.='<tr>'
								.'<td>'.$campo['clave_campo'].' (Externo)</td>'
								.'<td>'.$this->tipo_dato($campo['tipo_campo']).'</td>'
								.'<td>'.$campo['indice'].'</td>'
								.'<td>'.$campo['visible'].'</td>'
								.'<td>'.$campo['editable'].'</td>'
								.'<td>'.$campo['requerido'].'</td>'
								.'<td>'.$campo['busqueda'].'</td>'
							.'</tr>';
				}
			}
			$tabla_row.='
					</tbody>
				</table>';
				echo $tabla_row;
		}
		if($this->tipo_tabla==2)
		{
			if(isset($this->subtablas))
			{
				echo "<h1>Subtablas: "."</h1>";
				foreach ($this->subtablas as $tabla )
				{
					echo "<h2>+".$tabla['clave_tabla']."</h2>";

				}
					$tabla_row='
						<table class="table table-sm table-hover table-stripe">
							<thead>
								<tr>
								<th>Clave</th>
								<th>Tipo</th>
								<th>Indice</th>
								<th>Visible</th>
								<th>Editable</th>
								<th>Requerido</th>
								<th>Busqueda</th>
								</tr>
							</thead>
							<tbody>';
					
					foreach ($this->indice as $campo)
					{
						$tabla_row.='<tr>'
									.'<td>'.$campo['clave_campo'].'</td>'
									.'<td>'.$this->tipo_dato($campo['tipo_campo']).'</td>'
									.'<td>'.$campo['indice'].'</td>'
									.'<td>'.$campo['visible'].'</td>'
									.'<td>'.$campo['editable'].'</td>'
									.'<td>'.$campo['requerido'].'</td>'
									.'<td>'.$campo['busqueda'].'</td>'
								.'</tr>';
					}

					foreach ($this->campos as $campo)
					{
						$tabla_row.='<tr>'
									.'<td>'.$campo['clave_campo'].'</td>'
									.'<td>'.$this->tipo_dato($campo['tipo_campo']).'</td>'
									.'<td>'.$campo['indice'].'</td>'
									.'<td>'.$campo['visible'].'</td>'
									.'<td>'.$campo['editable'].'</td>'
									.'<td>'.$campo['requerido'].'</td>'
									.'<td>'.$campo['busqueda'].'</td>'
								.'</tr>';
					}

					$tabla_row.='
							</tbody>
						</table>';
						echo $tabla_row;
				}
		}
	}
	
	public function formatos_permitidos_sin_comillas($jpeg, $png ,$gif ,$tif ,$svg ,$eps ,$pdf ,$xlsx, $xlsm, $doc ,$docx ,$dwg ,$txt ,$zip ,$rar)
	{
		$cadena='';
		if($jpeg==1)
			$cadena.= '.jpeg, .jpg, ';
		if($png ==1)
			$cadena.= '.png, ';
		if($gif ==1)
			$cadena.= '.gif, ';
		if($tif ==1)
			$cadena.= '.tif, ';
		if($svg ==1)
			$cadena.= '.svg, ';
		if($eps ==1)
			$cadena.= '.eps, ';
		if($pdf ==1)
			$cadena.= '.pdf, ';
		if($xlsx==1)
			$cadena.= '.xlsx, ';
		if($xlsm==1)
			$cadena.= '.xlsm, ';
		if($doc ==1)
			$cadena.= '.doc, ';
		if($docx==1)
			$cadena.= '.docx, ';
		if($dwg ==1)
			$cadena.= '.dwg, ';
		if($txt ==1)
			$cadena.= '.txt, ';
		if($zip ==1)
			$cadena.= '.zip, ';
		if($rar ==1)
			$cadena.= '.rar, ';
		$cadena=substr($cadena, 0, -2);
		return $cadena;
	}

	public function formatos_permitidos($jpeg, $png ,$gif ,$tif ,$svg ,$eps ,$pdf ,$xlsx, $xlsm, $doc ,$docx ,$dwg ,$txt ,$zip ,$rar)
	{
		$cadena='';
		if($jpeg==1)
			$cadena.= '"jpeg", "jpg", ';
		if($png ==1)
			$cadena.= '"png", ';
		if($gif ==1)
			$cadena.= '"gif", ';
		if($tif ==1)
			$cadena.= '"tif", ';
		if($svg ==1)
			$cadena.= '"svg", ';
		if($eps ==1)
			$cadena.= '"eps", ';
		if($pdf ==1)
			$cadena.= '"pdf", ';
		if($xlsx==1)
			$cadena.= '"xlsx", ';
		if($xlsm==1)
			$cadena.= '"xlsm", ';
		if($doc ==1)
			$cadena.= '"doc", ';
		if($docx==1)
			$cadena.= '"docx", ';
		if($dwg ==1)
			$cadena.= '"dwg", ';
		if($txt ==1)
			$cadena.= '"txt", ';
		if($zip ==1)
			$cadena.= '"zip", ';
		if($rar ==1)
			$cadena.= '"rar", ';
		$cadena=substr($cadena, 0, -2);
		return $cadena;
	}

	private function tipo_dato($val)
	{
		switch ($val) 
		{
			case 1: return 'INT'; break;
			case 2: return 'CHAR'; break;
			case 3: return 'DATE'; break;
			case 4: return 'DATETIME'; break;
			case 5: return 'TEXT'; break;
			case 6: return 'FILE'; break;
			case 7: return 'BOOLEAN'; break;
			case 8: return 'DOUBLE'; break;
			default: break;
		}	
	}

	public function icono($id)
	{
		switch ($id)
		{
			case '1':  return "fas fa-xs fa-asterisk"; break;
			case '2':  return "fas fa-xs fa-plus"; break;
			case '3':  return "fas fa-xs fa-euro"; break;
			case '4':  return "fas fa-xs fa-eur"; break;
			case '5':  return "fas fa-xs fa-minus"; break;
			case '6':  return "fas fa-xs fa-cloud"; break;
			case '7':  return "fas fa-xs fa-envelope"; break;
			case '8':  return "fas fa-xs fa-pencil"; break;
			case '9':  return "fas fa-xs fa-glass"; break;
			case '10':  return "fas fa-xs fa-music"; break;
			case '11':  return "fas fa-xs fa-search"; break;
			case '12':  return "fas fa-xs fa-heart"; break;
			case '13':  return "fas fa-xs fa-star"; break;
			case '14':  return "fas fa-xs fa-star-empty"; break;
			case '15':  return "fas fa-xs fa-user"; break;
			case '16':  return "fas fa-xs fa-film"; break;
			case '17':  return "fas fa-xs fa-th-large"; break;
			case '18':  return "fas fa-xs fa-th"; break;
			case '19':  return "fas fa-xs fa-th-list"; break;
			case '20':  return "fas fa-xs fa-ok"; break;
			case '21':  return "fas fa-xs fa-remove"; break;
			case '22':  return "fas fa-xs fa-zoom-in"; break;
			case '23':  return "fas fa-xs fa-zoom-out"; break;
			case '24':  return "fas fa-xs fa-off"; break;
			case '25':  return "fas fa-xs fa-signal"; break;
			case '26':  return "fas fa-xs fa-cog"; break;
			case '27':  return "fas fa-xs fa-trash"; break;
			case '28':  return "fas fa-xs fa-home"; break;
			case '29':  return "fas fa-xs fa-file"; break;
			case '30':  return "fas fa-xs fa-time"; break;
			case '31':  return "fas fa-xs fa-road"; break;
			case '32':  return "fas fa-xs fa-download-alt"; break;
			case '33':  return "fas fa-xs fa-download"; break;
			case '34':  return "fas fa-xs fa-upload"; break;
			case '35':  return "fas fa-xs fa-inbox"; break;
			case '36':  return "fas fa-xs fa-play-circle"; break;
			case '37':  return "fas fa-xs fa-repeat"; break;
			case '38':  return "fas fa-xs fa-refresh"; break;
			case '39':  return "fas fa-xs fa-list-alt"; break;
			case '40':  return "fas fa-xs fa-lock"; break;
			case '41':  return "fas fa-xs fa-flag"; break;
			case '42':  return "fas fa-xs fa-headphones"; break;
			case '43':  return "fas fa-xs fa-volume-off"; break;
			case '44':  return "fas fa-xs fa-volume-down"; break;
			case '45':  return "fas fa-xs fa-volume-up"; break;
			case '46':  return "fas fa-xs fa-qrcode"; break;
			case '47':  return "fas fa-xs fa-barcode"; break;
			case '48':  return "fas fa-xs fa-tag"; break;
			case '49':  return "fas fa-xs fa-tags"; break;
			case '50':  return "fas fa-xs fa-book"; break;
			case '51':  return "fas fa-xs fa-bookmark"; break;
			case '52':  return "fas fa-xs fa-print"; break;
			case '53':  return "fas fa-xs fa-camera"; break;
			case '54':  return "fas fa-xs fa-font"; break;
			case '55':  return "fas fa-xs fa-bold"; break;
			case '56':  return "fas fa-xs fa-italic"; break;
			case '57':  return "fas fa-xs fa-text-height"; break;
			case '58':  return "fas fa-xs fa-text-width"; break;
			case '59':  return "fas fa-xs fa-align-left"; break;
			case '60':  return "fas fa-xs fa-align-center"; break;
			case '61':  return "fas fa-xs fa-align-right"; break;
			case '62':  return "fas fa-xs fa-align-justify"; break;
			case '63':  return "fas fa-xs fa-list"; break;
			case '64':  return "fas fa-xs fa-indent-left"; break;
			case '65':  return "fas fa-xs fa-indent-right"; break;
			case '66':  return "fas fa-xs fa-facetime-video"; break;
			case '67':  return "fas fa-xs fa-picture"; break;
			case '68':  return "fas fa-xs fa-map-marker"; break;
			case '69':  return "fas fa-xs fa-adjust"; break;
			case '70':  return "fas fa-xs fa-tint"; break;
			case '71':  return "fas fa-xs fa-edit"; break;
			case '72':  return "fas fa-xs fa-share"; break;
			case '73':  return "fas fa-xs fa-check"; break;
			case '74':  return "fas fa-xs fa-move"; break;
			case '75':  return "fas fa-xs fa-step-backward"; break;
			case '76':  return "fas fa-xs fa-fast-backward"; break;
			case '77':  return "fas fa-xs fa-backward"; break;
			case '78':  return "fas fa-xs fa-play"; break;
			case '79':  return "fas fa-xs fa-pause"; break;
			case '80':  return "fas fa-xs fa-stop"; break;
			case '81':  return "fas fa-xs fa-forward"; break;
			case '82':  return "fas fa-xs fa-fast-forward"; break;
			case '83':  return "fas fa-xs fa-step-forward"; break;
			case '84':  return "fas fa-xs fa-eject"; break;
			case '85':  return "fas fa-xs fa-chevron-left"; break;
			case '86':  return "fas fa-xs fa-chevron-right"; break;
			case '87':  return "fas fa-xs fa-plus-sign"; break;
			case '88':  return "fas fa-xs fa-minus-sign"; break;
			case '89':  return "fas fa-xs fa-remove-sign"; break;
			case '90':  return "fas fa-xs fa-ok-sign"; break;
			case '91':  return "fas fa-xs fa-question-sign"; break;
			case '92':  return "fas fa-xs fa-info-sign"; break;
			case '93':  return "fas fa-xs fa-screenshot"; break;
			case '94':  return "fas fa-xs fa-remove-circle"; break;
			case '95':  return "fas fa-xs fa-ok-circle"; break;
			case '96':  return "fas fa-xs fa-ban-circle"; break;
			case '97':  return "fas fa-xs fa-arrow-left"; break;
			case '98':  return "fas fa-xs fa-arrow-right"; break;
			case '99':  return "fas fa-xs fa-arrow-up"; break;
			case '100':  return "fas fa-xs fa-arrow-down"; break;
			case '101':  return "fas fa-xs fa-share-alt"; break;
			case '102':  return "fas fa-xs fa-resize-full"; break;
			case '103':  return "fas fa-xs fa-resize-small"; break;
			case '104':  return "fas fa-xs fa-exclamation-sign"; break;
			case '105':  return "fas fa-xs fa-gift"; break;
			case '106':  return "fas fa-xs fa-leaf"; break;
			case '107':  return "fas fa-xs fa-fire"; break;
			case '108':  return "fas fa-xs fa-eye-open"; break;
			case '109':  return "fas fa-xs fa-eye-close"; break;
			case '110':  return "fas fa-xs fa-warning-sign"; break;
			case '111':  return "fas fa-xs fa-plane"; break;
			case '112':  return "fas fa-xs fa-calendar"; break;
			case '113':  return "fas fa-xs fa-random"; break;
			case '114':  return "fas fa-xs fa-comment"; break;
			case '115':  return "fas fa-xs fa-magnet"; break;
			case '116':  return "fas fa-xs fa-chevron-up"; break;
			case '117':  return "fas fa-xs fa-chevron-down"; break;
			case '118':  return "fas fa-xs fa-retweet"; break;
			case '119':  return "fas fa-xs fa-shopping-cart"; break;
			case '120':  return "fas fa-xs fa-folder-close"; break;
			case '121':  return "fas fa-xs fa-folder-open"; break;
			case '122':  return "fas fa-xs fa-resize-vertical"; break;
			case '123':  return "fas fa-xs fa-resize-horizontal"; break;
			case '124':  return "fas fa-xs fa-hdd"; break;
			case '125':  return "fas fa-xs fa-bullhorn"; break;
			case '126':  return "fas fa-xs fa-bell"; break;
			case '127':  return "fas fa-xs fa-certificate"; break;
			case '128':  return "fas fa-xs fa-thumbs-up"; break;
			case '129':  return "fas fa-xs fa-thumbs-down"; break;
			case '130':  return "fas fa-xs fa-hand-right"; break;
			case '131':  return "fas fa-xs fa-hand-left"; break;
			case '132':  return "fas fa-xs fa-hand-up"; break;
			case '133':  return "fas fa-xs fa-hand-down"; break;
			case '134':  return "fas fa-xs fa-circle-arrow-right"; break;
			case '135':  return "fas fa-xs fa-circle-arrow-left"; break;
			case '136':  return "fas fa-xs fa-circle-arrow-up"; break;
			case '137':  return "fas fa-xs fa-circle-arrow-down"; break;
			case '138':  return "fas fa-xs fa-globe"; break;
			case '139':  return "fas fa-xs fa-wrench"; break;
			case '140':  return "fas fa-xs fa-tasks"; break;
			case '141':  return "fas fa-xs fa-filter"; break;
			case '142':  return "fas fa-xs fa-briefcase"; break;
			case '143':  return "fas fa-xs fa-fullscreen"; break;
			case '144':  return "fas fa-xs fa-dashboard"; break;
			case '145':  return "fas fa-xs fa-paperclip"; break;
			case '146':  return "fas fa-xs fa-heart-empty"; break;
			case '147':  return "fas fa-xs fa-link"; break;
			case '148':  return "fas fa-xs fa-phone"; break;
			case '149':  return "fas fa-xs fa-pushpin"; break;
			case '150':  return "fas fa-xs fa-usd"; break;
			case '151':  return "fas fa-xs fa-gbp"; break;
			case '152':  return "fas fa-xs fa-sort"; break;
			case '153':  return "fas fa-xs fa-sort-by-alphabet"; break;
			case '154':  return "fas fa-xs fa-sort-by-alphabet-alt"; break;
			case '155':  return "fas fa-xs fa-sort-by-order"; break;
			case '156':  return "fas fa-xs fa-sort-by-order-alt"; break;
			case '157':  return "fas fa-xs fa-sort-by-attributes"; break;
			case '158':  return "fas fa-xs fa-sort-by-attributes-alt"; break;
			case '159':  return "fas fa-xs fa-unchecked"; break;
			case '160':  return "fas fa-xs fa-expand"; break;
			case '161':  return "fas fa-xs fa-collapse-down"; break;
			case '162':  return "fas fa-xs fa-collapse-up"; break;
			case '163':  return "fas fa-xs fa-log-in"; break;
			case '164':  return "fas fa-xs fa-flash"; break;
			case '165':  return "fas fa-xs fa-log-out"; break;
			case '166':  return "fas fa-xs fa-new-window"; break;
			case '167':  return "fas fa-xs fa-record"; break;
			case '168':  return "fas fa-xs fa-save"; break;
			case '169':  return "fas fa-xs fa-open"; break;
			case '170':  return "fas fa-xs fa-saved"; break;
			case '171':  return "fas fa-xs fa-import"; break;
			case '172':  return "fas fa-xs fa-export"; break;
			case '173':  return "fas fa-xs fa-send"; break;
			case '174':  return "fas fa-xs fa-floppy-disk"; break;
			case '175':  return "fas fa-xs fa-floppy-saved"; break;
			case '176':  return "fas fa-xs fa-floppy-remove"; break;
			case '177':  return "fas fa-xs fa-floppy-save"; break;
			case '178':  return "fas fa-xs fa-floppy-open"; break;
			case '179':  return "fas fa-xs fa-credit-card"; break;
			case '180':  return "fas fa-xs fa-transfer"; break;
			case '181':  return "fas fa-xs fa-cutlery"; break;
			case '182':  return "fas fa-xs fa-header"; break;
			case '183':  return "fas fa-xs fa-compressed"; break;
			case '184':  return "fas fa-xs fa-earphone"; break;
			case '185':  return "fas fa-xs fa-phone-alt"; break;
			case '186':  return "fas fa-xs fa-tower"; break;
			case '187':  return "fas fa-xs fa-stats"; break;
			case '188':  return "fas fa-xs fa-sd-video"; break;
			case '189':  return "fas fa-xs fa-hd-video"; break;
			case '190':  return "fas fa-xs fa-subtitles"; break;
			case '191':  return "fas fa-xs fa-sound-stereo"; break;
			case '192':  return "fas fa-xs fa-sound-dolby"; break;
			case '193':  return "fas fa-xs fa-sound-5-1"; break;
			case '194':  return "fas fa-xs fa-sound-6-1"; break;
			case '195':  return "fas fa-xs fa-sound-7-1"; break;
			case '196':  return "fas fa-xs fa-copyright-mark"; break;
			case '197':  return "fas fa-xs fa-registration-mark"; break;
			case '198':  return "fas fa-xs fa-cloud-download"; break;
			case '199':  return "fas fa-xs fa-cloud-upload"; break;
			case '200':  return "fas fa-xs fa-tree-conifer"; break;
			case '201':  return "fas fa-xs fa-tree-deciduous"; break;
			case '202':  return "fas fa-xs fa-cd"; break;
			case '203':  return "fas fa-xs fa-save-file"; break;
			case '204':  return "fas fa-xs fa-open-file"; break;
			case '205':  return "fas fa-xs fa-level-up"; break;
			case '206':  return "fas fa-xs fa-copy"; break;
			case '207':  return "fas fa-xs fa-paste"; break;
			case '208':  return "fas fa-xs fa-alert"; break;
			case '209':  return "fas fa-xs fa-equalizer"; break;
			case '210':  return "fas fa-xs fa-king"; break;
			case '211':  return "fas fa-xs fa-queen"; break;
			case '212':  return "fas fa-xs fa-pawn"; break;
			case '213':  return "fas fa-xs fa-bishop"; break;
			case '214':  return "fas fa-xs fa-knight"; break;
			case '215':  return "fas fa-xs fa-baby-formula"; break;
			case '216':  return "fas fa-xs fa-tent"; break;
			case '217':  return "fas fa-xs fa-blackboard"; break;
			case '218':  return "fas fa-xs fa-bed"; break;
			case '219':  return "fas fa-xs fa-apple"; break;
			case '220':  return "fas fa-xs fa-erase"; break;
			case '221':  return "fas fa-xs fa-hourglass"; break;
			case '222':  return "fas fa-xs fa-lamp"; break;
			case '223':  return "fas fa-xs fa-duplicate"; break;
			case '224':  return "fas fa-xs fa-piggy-bank"; break;
			case '225':  return "fas fa-xs fa-scissors"; break;
			case '226':  return "fas fa-xs fa-bitcoin"; break;
			case '227':  return "fas fa-xs fa-btc"; break;
			case '228':  return "fas fa-xs fa-xbt"; break;
			case '229':  return "fas fa-xs fa-yen"; break;
			case '230':  return "fas fa-xs fa-jpy"; break;
			case '231':  return "fas fa-xs fa-ruble"; break;
			case '232':  return "fas fa-xs fa-rub"; break;
			case '233':  return "fas fa-xs fa-scale"; break;
			case '234':  return "fas fa-xs fa-ice-lolly"; break;
			case '235':  return "fas fa-xs fa-ice-lolly-tasted"; break;
			case '236':  return "fas fa-xs fa-education"; break;
			case '237':  return "fas fa-xs fa-option-horizontal"; break;
			case '238':  return "fas fa-xs fa-option-vertical"; break;
			case '239':  return "fas fa-xs fa-menu-hamburger"; break;
			case '240':  return "fas fa-xs fa-modal-window"; break;
			case '241':  return "fas fa-xs fa-oil"; break;
			case '242':  return "fas fa-xs fa-grain"; break;
			case '243':  return "fas fa-xs fa-sunglasses"; break;
			case '244':  return "fas fa-xs fa-text-size"; break;
			case '245':  return "fas fa-xs fa-text-color"; break;
			case '246':  return "fas fa-xs fa-text-background"; break;
			case '247':  return "fas fa-xs fa-object-align-top"; break;
			case '248':  return "fas fa-xs fa-object-align-bottom"; break;
			case '249':  return "fas fa-xs fa-object-align-horizontal"; break;
			case '250':  return "fas fa-xs fa-object-align-left"; break;
			case '251':  return "fas fa-xs fa-object-align-vertical"; break;
			case '252':  return "fas fa-xs fa-object-align-right"; break;
			case '253':  return "fas fa-xs fa-triangle-right"; break;
			case '254':  return "fas fa-xs fa-triangle-left"; break;
			case '255':  return "fas fa-xs fa-triangle-bottom"; break;
			case '256':  return "fas fa-xs fa-triangle-top"; break;
			case '257':  return "fas fa-xs fa-console"; break;
			case '258':  return "fas fa-xs fa-superscript"; break;
			case '259':  return "fas fa-xs fa-subscript"; break;
			case '260':  return "fas fa-xs fa-menu-left"; break;
			case '261':  return "fas fa-xs fa-menu-right"; break;
			case '262':  return "fas fa-xs fa-menu-down"; break;
			case '263':  return "fas fa-xs fa-menu-up"; break;
			default: break;
		}
	}

	public function action_add_modal($url)
	{

		$out = fopen($url.'/'.$this->clave_tabla."_add_action.php", "w+");
		fwrite($out,'<?php 

		echo "<pre>";
		var_dump($_POST);
		echo "</pre>";
		echo "<pre>";
		var_dump($_FILES);
		echo "</pre>";
		'.PHP_EOL);
		$aux='';
		$i=0;
		foreach ($this->campos as $campo)
		{
			if($campo['requerido']==1)
			{
				fwrite($out,'if (empty($_POST["add_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_campo"])){ $errors[] = "Error en add_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_campo"; }'.PHP_EOL);
			}
		}

		fwrite($out,'if('.PHP_EOL);
		foreach ($this->campos as $campo )
		{
			if($campo['editable']==1 && $campo['requerido']==1)
			{
				if($i==0)
				{
					fwrite($out,'!empty($_POST["add_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_campo"]) '.PHP_EOL);
				}
				else
				{
					fwrite($out,'&& !empty($_POST["add_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_campo"]) '.PHP_EOL);
				}

				$i=$i+1;
			}
		}
		if(isset($this->indices_externos))
		{
			foreach ($this->indices_externos as $campo )
			{
				if($i==0)
				{
					fwrite($out,'!empty($_POST["add_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_'.$this->nombre_origen.'_externo"]) '.PHP_EOL);
				}
				else
				{
					fwrite($out,'&& !empty($_POST["add_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_'.$this->nombre_origen.'_externo"]) '.PHP_EOL);
				}

				$i=$i+1;
			}
		}
		if($i==0)
			fwrite($out,' 1==1 '.PHP_EOL);

		fwrite($out,')'.PHP_EOL);
		fwrite($out,'{'.PHP_EOL);
		fwrite($out,'require_once ("../config/db.php");'.PHP_EOL);
		fwrite($out,'require_once ("../config/conexion.php");'.PHP_EOL);


		
		$i=0;
		$consulta='$sql="INSERT INTO '.$this->clave_tabla.' (';
		$consulta2='';
		foreach ($this->indice as $campo)
		{
			$consulta2.="null, ";
			$consulta.=$campo['clave_campo'].", ";
			$aux=$this->clave_tabla.'_'.$campo['clave_campo'];
		}

		if(isset($this->indices_externos))
		{
			foreach ($this->indices_externos as $campo )
			{
				switch ($campo['tipo_campo']) {
					case 1://INTEGER
						$aux=$this->clave_tabla.'_'.$campo['clave_campo'].'_'.$this->nombre_origen.'_externo';
						fwrite($out, '$add_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_'.$this->nombre_origen.'_externo=intval($_POST["add_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_'.$this->nombre_origen.'_externo"]);'.PHP_EOL);
						break;

					case 2://VARCHAR
						$aux=$this->clave_tabla.'_'.$campo['clave_campo'].'_'.$this->nombre_origen.'_externo';
						fwrite($out, '$add_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_'.$this->nombre_origen.'_externo'.'=mysqli_real_escape_string($con,(strip_tags($_POST["add_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_'.$this->nombre_origen.'_externo'.'"],ENT_QUOTES)));'.PHP_EOL);
						break;

					case 3://DATE
						$aux=$this->clave_tabla.'_'.$campo['clave_campo'].'_'.$this->nombre_origen.'_externo';
						fwrite($out, '$add_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_'.$this->nombre_origen.'_externo'.'=mysqli_real_escape_string($con,(strip_tags($_POST["add_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_'.$this->nombre_origen.'_externo'.'"],ENT_QUOTES)));'.PHP_EOL);
						break;

					case 4://DATETIME
						$aux=$this->clave_tabla.'_'.$campo['clave_campo'].'_'.$this->nombre_origen.'_externo';
						fwrite($out, '$add_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_'.$this->nombre_origen.'_externo'.'=mysqli_real_escape_string($con,(strip_tags($_POST["add_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_'.$this->nombre_origen.'_externo'.'"],ENT_QUOTES)));'.PHP_EOL);
						break;

					case 5://TEXT
						$aux=$this->clave_tabla.'_'.$campo['clave_campo'].'_'.$this->nombre_origen.'_externo';
						fwrite($out, '$add_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_'.$this->nombre_origen.'_externo'.'=mysqli_real_escape_string($con,(strip_tags($_POST["add_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_'.$this->nombre_origen.'_externo'.'"],ENT_QUOTES)));'.PHP_EOL);
						break;

					case 6://FILE
						$aux=$this->clave_tabla.'_'.$campo['clave_campo'].'_'.$this->nombre_origen.'_externo';
						fwrite($out, '$add_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_'.$this->nombre_origen.'_externo'.'=mysqli_real_escape_string($con,(strip_tags($_POST["add_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_'.$this->nombre_origen.'_externo'.'"],ENT_QUOTES)));'.PHP_EOL);
						break;

					case 7://BOOLEAN
						$aux=$this->clave_tabla.'_'.$campo['clave_campo'].'_'.$this->nombre_origen.'_externo';
						fwrite($out, '$add_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_'.$this->nombre_origen.'_externo'.'=intval($_POST["add_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_'.$this->nombre_origen.'_externo'.'"]);'.PHP_EOL);
						break;

					case 8://DOUBLE
						$aux=$this->clave_tabla.'_'.$campo['clave_campo'].'_'.$this->nombre_origen.'_externo';
						fwrite($out, '$add_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_'.$this->nombre_origen.'_externo'.'=doubleval($_POST["add_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_'.$this->nombre_origen.'_externo'.'"]);'.PHP_EOL);
						break;
					
					default:
						break;
				}
				$consulta.=$this->clave_tabla.'_'.$campo['clave_campo'].'_'.$this->nombre_origen.", ";
				$consulta2.="'\$add_".$this->clave_tabla.'_'.$campo['clave_campo'].'_'.$this->nombre_origen.'_externo'."', ";
			}
		}



		foreach ($this->campos as $campo)
		{
			if($campo['editable']==1)
			{

				switch ($campo['tipo_campo']) {
					case 1://INTEGER
						fwrite($out, '$add_'.$campo['clave_campo'].'_campo=intval($_POST["add_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_campo"]);'.PHP_EOL);
						$consulta.=$campo['clave_campo'].", ";
						$consulta2.="'\$add_".$campo['clave_campo']."_campo', ";
						break;

					case 2://VARCHAR
						fwrite($out, '$add_'.$campo['clave_campo'].'_campo=mysqli_real_escape_string($con,(strip_tags($_POST["add_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_campo"],ENT_QUOTES)));'.PHP_EOL);
						$consulta.=$campo['clave_campo'].", ";
						$consulta2.="'\$add_".$campo['clave_campo']."_campo', ";
						break;

					case 3://DATE
						fwrite($out, '$add_'.$campo['clave_campo'].'_campo=mysqli_real_escape_string($con,(strip_tags($_POST["add_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_campo"],ENT_QUOTES)));'.PHP_EOL);
						$consulta.=$campo['clave_campo'].", ";
						$consulta2.="'\$add_".$campo['clave_campo']."_campo', ";
						break;

					case 4://DATETIME
						fwrite($out, '$add_'.$campo['clave_campo'].'_campo=mysqli_real_escape_string($con,(strip_tags($_POST["add_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_campo"],ENT_QUOTES)));'.PHP_EOL);
						$consulta.=$campo['clave_campo'].", ";
						$consulta2.="'\$add_".$campo['clave_campo']."_campo', ";
						break;

					case 5://TEXT
						fwrite($out, '$add_'.$campo['clave_campo'].'_campo=mysqli_real_escape_string($con,(strip_tags($_POST["add_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_campo"],ENT_QUOTES)));'.PHP_EOL);
						$consulta.=$campo['clave_campo'].", ";
						$consulta2.="'\$add_".$campo['clave_campo']."_campo', ";
						break;

					case 6://FILE
					fwrite($out, '
					if(($_FILES["add_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_campo"]['.'"name"'.'])!="")
					{
						$upload_dir'.$campo['clave_campo'].' = "../'.$this->clave_tabla.'/'.$campo['carpeta'].'/";
						$imgName'.$campo['clave_campo'].' = $_FILES["add_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_campo"]["name"];
						$imgTmp'.$campo['clave_campo'].' = $_FILES["add_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_campo"]["tmp_name"];
						$imgSize'.$campo['clave_campo'].' = $_FILES["add_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_campo"]["size"];
						$imgExt'.$campo['clave_campo'].' = strtolower(pathinfo($imgName'.$campo['clave_campo'].', PATHINFO_EXTENSION));
						$allowExt'.$campo['clave_campo'].' = array('.$this->formatos_permitidos($campo['jpeg'] ,$campo['png'] ,$campo['gif'] ,$campo['tif'] ,$campo['svg'] ,$campo['eps'] ,$campo['pdf'] ,$campo['xlsx'],$campo['xlsm'], $campo['doc'] ,$campo['docx'],$campo['dwg'] ,$campo['txt'] ,$campo['zip'] ,$campo['rar']).');//array("jpeg", "jpg", "png", "gif");
						$userPic'.$campo['clave_campo'].' = date("YmdGis").".".$imgExt'.$campo['clave_campo'].';
						$names'.$campo['clave_campo'].' = $userPic'.$campo['clave_campo'].';

						if(in_array($imgExt'.$campo['clave_campo'].', $allowExt'.$campo['clave_campo'].'))
						{
							if($imgSize'.$campo['clave_campo'].' < 5000000)
							{
								move_uploaded_file($imgTmp'.$campo['clave_campo'].' ,$upload_dir'.$campo['clave_campo'].'.$userPic'.$campo['clave_campo'].');
							}
							else
							{
								$errors [] = "Archivo sobrepasa 5Mb";
							}
						}
						else
						{
							$errors [] = "Extencion de archivo no permitida";
						}
					}
					else
					{
						$names'.$campo['clave_campo'].' = "";
					}'.PHP_EOL);
											fwrite($out, '
						$add_'.$campo['clave_campo'].'_campo=$names'.$campo['clave_campo'].';'.PHP_EOL);
						$consulta.=$campo['clave_campo'].", ";
						$consulta2.="'\$add_".$campo['clave_campo']."_campo', ";
						break;

					case 7://BOOLEAN
						fwrite($out, '$add_'.$campo['clave_campo'].'_campo= (mysqli_real_escape_string($con,(strip_tags($_POST["add_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_campo"],ENT_QUOTES)))=="on")?1:0;'.PHP_EOL);
						$consulta.=$campo['clave_campo'].", ";
						$consulta2.="'\$add_".$campo['clave_campo']."_campo', ";
						break;

					case 8://DOUBLE
						fwrite($out, '$add_'.$campo['clave_campo'].'_campo=doubleval($_POST["add_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_campo"]);'.PHP_EOL);
						$consulta.=$campo['clave_campo'].", ";
						$consulta2.="'\$add_".$campo['clave_campo']."_campo', ";
						break;
					
					default:
						# code...
						break;
					}
			}
		}




		$consulta=substr($consulta, 0, -2);
		$consulta2=substr($consulta2, 0, -2);
		fwrite($out, $consulta.') VALUES ('.$consulta2.');";'.PHP_EOL);
		fwrite($out,'$query_insert = mysqli_query($con,$sql);'.PHP_EOL);
		fwrite($out, '

				$last_id = mysqli_insert_id($con);
				if ($query_insert)
				{
					$messages[] = "concepto ha sido actualizado satisfactoriamente.";
				} else{
					$errors []= "Lo siento algo ha salido mal intenta nuevamente.".mysqli_error($con);
				}
			} else {
				$errors []= "Error desconocido.";
			}
			
			if (isset($errors))
			{
				
				?>
				<div class="alert alert-danger" role="alert">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
						<strong>Error!</strong> 
						<?php
							foreach ($errors as $error)
							{
								echo $error;
							}
							?>
				</div>
				<?php
				}
				if (isset($messages))
				{
					
					?>
					<div class="alert alert-success" role="alert">
							<button type="button" class="close" data-dismiss="alert">&times;</button>
							<strong>¡Bien hecho!</strong>
							<?php
								foreach ($messages as $message)
								{
									echo $message;
								}
								?>
					</div>
				<?php  '.PHP_EOL);
			if(!isset($this->archivos))
			{

				fwrite($out, "}");
			}
			else
			{
				if($this->tipo_tabla==1)
				{
				fwrite($out, "header('location: ../".$this->clave_tabla."/VER.PHP?".$this->clave_tabla."='.\$last_id);
					");
				}
				if($this->tipo_tabla==2)
				{
				fwrite($out,'$add_'.$aux.'=$_POST["add_'.$aux.'"];'.PHP_EOL);
				fwrite($out, "header('location: ../".$this->nombre_origen."/VER.PHP?".$this->nombre_origen."='.\$add_".$aux.");
					}");
				}
				else
				{
					fwrite($out, "}");
				}
			}
			fwrite($out,'?>'.PHP_EOL);
	}
	
	public function action_edit_modal($url)
	{
		$aux2='';
		$aux='';
		foreach ($this->indice as $campo)
		{
			if($campo['indice']==1)
			{
				$orderby2=$campo['clave_campo'];
				$orderby=$this->clave_tabla."_".$campo['clave_campo']."_indice";
				break;
			}
		}
		$out = fopen($url.'/'.$this->clave_tabla."_edit_action.php", "w+");
		fwrite($out,'<?php echo "<pre>";
		var_dump($_POST);
		echo "</pre>";
		echo "<pre>";
		var_dump($_FILES);
		echo "</pre>";'.PHP_EOL);

		$i=0;
		$editable=0;

		fwrite($out,'if('.PHP_EOL);
		foreach ($this->campos as $campo )
		{

			if($campo['requerido']==1)
			{
				if($i==0)
				{
					fwrite($out,'!empty($_POST["edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_campo"]) '.PHP_EOL);
				}
				else
				{
					fwrite($out,'&& !empty($_POST["edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_campo"]) '.PHP_EOL);
				}
				$i=$i+1;
			}
		}

				

		if(isset($this->indices_externos))
		{
			foreach ($this->indices_externos as $campo )
			{
				if($i==0)
				{
					fwrite($out,'!empty($_POST["edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_'.$this->nombre_origen.'_externo"]) '.PHP_EOL);
				}
				else
				{
					fwrite($out,'&& !empty($_POST["edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_'.$this->nombre_origen.'_externo"]) '.PHP_EOL);
				}

				$i=$i+1;
			}
		}

		if($i==0)
			fwrite($out,' 1==1 '.PHP_EOL);


		fwrite($out,')'.PHP_EOL);
		fwrite($out,'{'.PHP_EOL);
		fwrite($out,'require_once ("../config/db.php");'.PHP_EOL);
		fwrite($out,'require_once ("../config/conexion.php");'.PHP_EOL);

		foreach ($this->indice as $campo) 
		{
			if($campo['requerido']==1)
			{
				fwrite($out,'if (empty($_POST["edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_indice"])){ $errors[] = "Error en edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_indice"; }'.PHP_EOL);
			}
		}

		foreach ($this->campos as $campo) 
		{
			if($campo['requerido']==1)
			{
				fwrite($out,'if (empty($_POST["edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_campo"])){ $errors[] = "Error en edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_campo"; }'.PHP_EOL);
			}
		}

		foreach ($this->indice as $campo)
		{
			fwrite($out, '$edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_indice'.'=mysqli_real_escape_string($con,(strip_tags($_POST["edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_indice"],ENT_QUOTES)));'.PHP_EOL);
		}
		
		if(isset($this->indices_externos))
		{
			foreach ($this->indices_externos as $campo )
			{
				switch ($campo['tipo_campo']) {
					case 1://INTEGER
						$aux2='edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_'.$this->nombre_origen.'_externo';
						fwrite($out, '$edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_'.$this->nombre_origen.'_externo'.'=intval($_POST["edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_'.$this->nombre_origen.'_externo'.'"]);'.PHP_EOL);
						break;

					case 2://VARCHAR
						$aux2='edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_'.$this->nombre_origen.'_externo';
						fwrite($out, '$edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_'.$this->nombre_origen.'_externo'.'=mysqli_real_escape_string($con,(strip_tags($_POST["edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_'.$this->nombre_origen.'_externo'.'"],ENT_QUOTES)));'.PHP_EOL);
						break;

					case 3://DATE
						$aux2='edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_'.$this->nombre_origen.'_externo';
						fwrite($out, '$edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_'.$this->nombre_origen.'_externo'.'=mysqli_real_escape_string($con,(strip_tags($_POST["edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_'.$this->nombre_origen.'_externo'.'"],ENT_QUOTES)));'.PHP_EOL);
						break;

					case 4://DATETIME
						$aux2='edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_'.$this->nombre_origen.'_externo';
						fwrite($out, '$edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_'.$this->nombre_origen.'_externo'.'=mysqli_real_escape_string($con,(strip_tags($_POST["edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_'.$this->nombre_origen.'_externo'.'"],ENT_QUOTES)));'.PHP_EOL);
						break;

					case 5://TEXT
						$aux2='edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_'.$this->nombre_origen.'_externo';
						fwrite($out, '$edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_'.$this->nombre_origen.'_externo'.'=mysqli_real_escape_string($con,(strip_tags($_POST["edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_'.$this->nombre_origen.'_externo'.'"],ENT_QUOTES)));'.PHP_EOL);
						break;

					case 6://FILE
						$aux2='edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_'.$this->nombre_origen.'_externo';
						fwrite($out, '$edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_'.$this->nombre_origen.'_externo'.'=mysqli_real_escape_string($con,(strip_tags($_POST["edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_'.$this->nombre_origen.'_externo'.'"],ENT_QUOTES)));'.PHP_EOL);
						break;

					case 7://BOOLEAN
						$aux2='edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_'.$this->nombre_origen.'_externo';
						fwrite($out, '$edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_'.$this->nombre_origen.'_externo'.'=intval($_POST["edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_'.$this->nombre_origen.'_externo'.'"]);'.PHP_EOL);
						break;

					case 8://DOUBLE
						$aux2='edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_'.$this->nombre_origen.'_externo';
						fwrite($out, '$edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_'.$this->nombre_origen.'_externo'.'=doubleval($_POST["edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_'.$this->nombre_origen.'"]);'.PHP_EOL);
						break;
					
					default:
						break;
				}
			}
		}

		$consulta='$sql="UPDATE '.$this->clave_tabla.' SET ';
		$i=0;
		foreach ($this->campos as $campo)
		{
			if($campo['editable']==1)
			{

				switch ($campo['tipo_campo']) {
					case 1://INTEGER
						fwrite($out, '$edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_campo'.'=intval($_POST["edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_campo"]);'.PHP_EOL);
						$consulta.=$campo['clave_campo']."='\$edit_".$this->clave_tabla.'_'.$campo['clave_campo'].'_campo'."', ";
						break;

					case 2://VARCHAR
						fwrite($out, '$edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_campo'.'=mysqli_real_escape_string($con,(strip_tags($_POST["edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_campo"],ENT_QUOTES)));'.PHP_EOL);
						$consulta.=$campo['clave_campo']."='\$edit_".$this->clave_tabla.'_'.$campo['clave_campo'].'_campo'."', ";
						break;

					case 3://DATE
						fwrite($out, '$edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_campo'.'=mysqli_real_escape_string($con,(strip_tags($_POST["edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_campo"],ENT_QUOTES)));'.PHP_EOL);
						$consulta.=$campo['clave_campo']."='\$edit_".$this->clave_tabla.'_'.$campo['clave_campo'].'_campo'."', ";
						break;

					case 4://DATETIME
						fwrite($out, '$edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_campo'.'=mysqli_real_escape_string($con,(strip_tags($_POST["edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_campo"],ENT_QUOTES)));'.PHP_EOL);
						$consulta.=$campo['clave_campo']."='\$edit_".$this->clave_tabla.'_'.$campo['clave_campo'].'_campo'."', ";
						break;

					case 5://TEXT
						fwrite($out, '$edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_campo'.'=mysqli_real_escape_string($con,(strip_tags($_POST["edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_campo"],ENT_QUOTES)));'.PHP_EOL);
						$consulta.=$campo['clave_campo']."='\$edit_".$this->clave_tabla.'_'.$campo['clave_campo'].'_campo'."', ";
						break;

					case 6://FILE
					fwrite($out, '
					if(($_FILES["edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_campo"]['.'"name"'.'])!="" && ($_FILES["edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_campo"]['.'"size"'.'])>0)
					{
						$upload_dir'.$campo['clave_campo'].' = "../'.$this->clave_tabla.'/'.$campo['carpeta'].'/";
						$imgName'.$campo['clave_campo'].' = $_FILES["edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_campo"]["name"];
						$imgTmp'.$campo['clave_campo'].' = $_FILES["edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_campo"]["tmp_name"];
						$imgSize'.$campo['clave_campo'].' = $_FILES["edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_campo"]["size"];
						$imgExt'.$campo['clave_campo'].' = strtolower(pathinfo($imgName'.$campo['clave_campo'].', PATHINFO_EXTENSION));
						$allowExt'.$campo['clave_campo'].' = array('.$this->formatos_permitidos($campo['jpeg'] ,$campo['png'] ,$campo['gif'] ,$campo['tif'] ,$campo['svg'] ,$campo['eps'] ,$campo['pdf'] ,$campo['xlsx'],$campo['xlsm'], $campo['doc'] ,$campo['docx'],$campo['dwg'] ,$campo['txt'] ,$campo['zip'] ,$campo['rar']).');//array("jpeg", "jpg", "png", "gif");
						$userPic'.$campo['clave_campo'].' = date("YmdGis").".".$imgExt'.$campo['clave_campo'].';
						$names'.$campo['clave_campo'].' = $userPic'.$campo['clave_campo'].';

						if(in_array($imgExt'.$campo['clave_campo'].', $allowExt'.$campo['clave_campo'].'))
						{
							if($imgSize'.$campo['clave_campo'].' < 5000000)
							{
								move_uploaded_file($imgTmp'.$campo['clave_campo'].' ,$upload_dir'.$campo['clave_campo'].'.$userPic'.$campo['clave_campo'].');');

								fwrite($out,'
								$consulta_file=mysqli_query($con,"SELECT * FROM '.$this->clave_tabla.' WHERE '.$orderby2.'=$edit_'.$orderby.';");
								$file=mysqli_fetch_array($consulta_file);');
								fwrite($out,'
									if(file_exists("../'.$this->clave_tabla.'/'.$campo['carpeta'].'/".$file["'.$campo['clave_campo'].'_campo"]))
										unlink( "../'.$this->clave_tabla.'/'.$campo['carpeta'].'/".$file["'.$campo['clave_campo'].'_campo"]);');
									
							fwrite($out,'
							}
							else
							{
								$errors [] = "Archivo sobrepasa 5Mb";
							}
						}
						else
						{
							$errors [] = "Extencion de archivo no permitida";
						}
					}
					else
					{	');


						$i=0;
						$aux2='';
						foreach ($this->indice as $campo1)
						{
							if($i==0)
							{
								$aux2.=' WHERE '.$campo1['clave_campo']." = '\$edit_".$this->clave_tabla.'_'.$campo1['clave_campo']."_indice'".'';
								$i=$i+1;
							}
							else
							{
								$aux2.=' AND '.$campo1['clave_campo']." = '\$edit_".$this->clave_tabla.'_'.$campo1['clave_campo']."_indice'".'';
								$i=$i+1;
							}
						}
						fwrite($out,'$sql="SELECT '.$this->clave_tabla.'.'.$campo['clave_campo'].' FROM '.$this->clave_tabla);
						fwrite($out,$aux2.';";'.PHP_EOL);
						$aux2='';

					fwrite($out,'

						$query = mysqli_query($con,$sql);
						$row=mysqli_fetch_array($query);
						$names'.$campo['clave_campo'].'=$row["'.$campo['clave_campo'].'"];
					}
						'.PHP_EOL);
						fwrite($out, '$edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_campo'.'=$names'.$campo['clave_campo'].';'.PHP_EOL);
						$consulta.=$campo['clave_campo']."='\$edit_".$this->clave_tabla.'_'.$campo['clave_campo'].'_campo'."', ";
						break;

					case 7://BOOLEAN
						fwrite($out, '$edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_campo'.'= (mysqli_real_escape_string($con,(strip_tags($_POST["edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_campo"],ENT_QUOTES)))=="on")?1:0;'.PHP_EOL);
						$consulta.=$campo['clave_campo']."='\$edit_".$this->clave_tabla.'_'.$campo['clave_campo'].'_campo'."', ";
						break;

					case 8://DOUBLE
						fwrite($out, '$edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_campo'.'=doubleval($_POST["edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_campo"]);'.PHP_EOL);
							$consulta.=$campo['clave_campo']."='\$edit_".$this->clave_tabla.'_'.$campo['clave_campo'].'_campo'."', ";
						break;
					
					default:
						# code...
						break;
				}
			}
		}


		$consulta=substr($consulta, 0, -2);
		$i=0;
		foreach ($this->indice as $campo)
		{
			if($i==0)
			{
				$consulta.=' WHERE '.$campo['clave_campo']." = '\$edit_".$this->clave_tabla.'_'.$campo['clave_campo'].'_indice'."'".'';
				$i=$i+1;
			}
			else
			{
				$consulta.=' AND '.$campo['clave_campo']." = '\$edit_".$this->clave_tabla.'_'.$campo['clave_campo'].'_indice'."'".'';
				$i=$i+1;
			}
		}

		
		fwrite($out, $consulta.'";'.PHP_EOL);
		fwrite($out,'$query_update = mysqli_query($con,$sql);'.PHP_EOL);
		fwrite($out, '
				if ($query_update)
				{
					$messages[] = "concepto ha sido actualizado satisfactoriamente.";
				} else{
					$errors []= "Lo siento algo ha salido mal intenta nuevamente.".mysqli_error($con);
				}
			} else {
				$errors []= "Error desconocido.";
			}
			
			if (isset($errors))
			{
				
				?>
				<div class="alert alert-danger" role="alert">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
						<strong>Error!</strong> 
						<?php
							foreach ($errors as $error)
							{
								echo $error;
							}
							?>
				</div>
				<?php
				}
				if (isset($messages))
				{
					
					?>
					<div class="alert alert-success" role="alert">
							<button type="button" class="close" data-dismiss="alert">&times;</button>
							<strong>¡Bien hecho!</strong>
							<?php
								foreach ($messages as $message)
								{
									echo $message;
								}
								?>
					</div>
				<?php  '.PHP_EOL);
			if(!isset($this->archivos))
			{

				fwrite($out, "}");
			}
			else
			{
				if($this->tipo_tabla==1)
				{
				fwrite($out, "header('location: ../".$this->clave_tabla."/');
					}");
				}
				else if($this->tipo_tabla==2)
				{

					foreach ($this->indices_externos as $campo )
					{
						$aux=$this->clave_tabla.'_'.$campo['clave_campo'].'_'.$this->nombre_origen;
					}
				fwrite($out,'$edit_'.$aux.'_externo=$_POST["edit_'.$aux.'_externo"];'.PHP_EOL);
				fwrite($out, "header('location: ../".$this->nombre_origen."/VER.PHP?".$this->nombre_origen."='.\$edit_".$aux."_externo);
					}");
				}
				else
				{
					fwrite($out, "}");
				}
			}
			fwrite($out,'?>'.PHP_EOL);
	}

	public function action_search($url)
	{
		foreach ($this->indice as $campo)
		{
			if($campo['indice']==1)
			{
				$orderby=$campo['clave_campo'];
				break;
			}
		}
		$busqueda='';
		foreach ($this->campos as $campo)
		{
			if($campo['busqueda']==1)
			{
				$busqueda.="'".$campo['clave_campo']."', ";
			}
		}
		$busqueda=substr($busqueda, 0, -2);
		$out = fopen($url.'/'.$this->clave_tabla."_search.php", "w+");
		fwrite($out,'<?php '.PHP_EOL);
		fwrite($out,'
		require_once ("../config/db.php");
		require_once ("../config/conexion.php");
		$action = (isset($_REQUEST["action"])&& $_REQUEST["action"] !=NULL)?$_REQUEST["action"]:"";
		if (isset($_GET["id"]))
		{
			$'.$orderby.'=intval($_GET["id"]);
			$query=mysqli_query($con, "select * from '.$this->clave_tabla.' WHERE '.$orderby.' = $'.$orderby.' ");
			$rw_user=mysqli_fetch_array($query);
			$count=$rw_user["'.$orderby.'"];


			$query=mysqli_query($con, "SELECT COUNT(*) as total FROM '.$this->clave_tabla.'");
			$rw_user=mysqli_fetch_array($query);
			');


			if($this->tipo_tabla==1)
			{
			fwrite($out, '
			if ($rw_user["total"]>1)
			{');
			}
			else
			{
			fwrite($out, '
			if ($rw_user["total"]>=1)
			{');
			}
				fwrite($out,'
				$consulta_file=mysqli_query($con,"SELECT * FROM '.$this->clave_tabla.' WHERE '.$orderby.'=$'.$orderby.';");
				$file=mysqli_fetch_array($consulta_file);');
					if(isset($this->archivos)){
					foreach ($this->archivos as $campo)
					{

				fwrite($out,'
				unlink( "../'.$this->clave_tabla.'/'.$campo['carpeta'].'/".$file["'.$campo['clave_campo'].'"]);');
					}
				}

				fwrite($out,'
				if ($delete1=mysqli_query($con,"DELETE FROM '.$this->clave_tabla.' WHERE '.$orderby.'=$'.$orderby.' LIMIT 1;"))
				{
				?>
				<div class="alert alert-success alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <strong>Aviso!</strong> Datos eliminados exitosamente.
				</div>
				<?php 
				}else {
					?>
					<div class="alert alert-danger alert-dismissible" role="alert">
					  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					  <strong>Error!</strong> Lo siento algo ha salido mal intenta nuevamente.
					</div>
					<?php
					
				}
				
			} else {
				?>
				<div class="alert alert-danger alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <strong>Error!</strong> No se puede eliminar todos los usuarios. 
				</div>
				<?php
			}
		}
		if($action == "ajax")
		{
		     $q = mysqli_real_escape_string($con,(strip_tags($_REQUEST["q"], ENT_QUOTES)));
			 $aColumns = array('.$busqueda.');
			 $sTable = "'.$this->clave_tabla.'";');


			if($this->tipo_tabla==2)
			{
				foreach ($this->indices_externos as $campo )
				{
				fwrite($out,'
				 $sWhere = "WHERE '.$this->clave_tabla.'_'.$campo['clave_campo'].'_'.$this->nombre_origen.'=".$_GET['."'".$this->nombre_origen."'".']'.''.';
				if ( $_GET["q"] != "" )
				{
					$sWhere = " AND (";
					for ( $i=0 ; $i<count($aColumns) ; $i++ )
					{
						$sWhere .= $aColumns[$i]." LIKE '."'".'%$q%'."'".' OR ";
					}
					$sWhere = substr_replace( $sWhere, "", -3 );
					$sWhere .= ")";
				}');
				}

			}
			else
			{

			fwrite($out,'
			 $sWhere = "";
			if ( $_GET["q"] != "" )
			{
				$sWhere = "WHERE (";
				for ( $i=0 ; $i<count($aColumns) ; $i++ )
				{
					$sWhere .= $aColumns[$i]." LIKE '."'".'%$q%'."'".' OR ";
				}
				$sWhere = substr_replace( $sWhere, "", -3 );
				$sWhere .= ")";
			}');
			}



			fwrite($out, '
			$sWhere.=" order by '.$orderby.' desc";
			include "pagination.php";
			$page = (isset($_REQUEST["page"]) && !empty($_REQUEST["page"]))?$_REQUEST["page"]:1;
			$per_page = 10;
			$adjacents  = 4;
			$offset = ($page - 1) * $per_page;
			$count_query   = mysqli_query($con, "SELECT count(*) AS numrows FROM $sTable  $sWhere");
			$row= mysqli_fetch_array($count_query);
			$numrows = $row["numrows"];
			$total_pages = ceil($numrows/$per_page);
			$reload = "../'.$this->clave_tabla.'/index.php";
			$sql="SELECT * FROM  $sTable $sWhere LIMIT $offset, $per_page";
			//echo $sql;
			$query = mysqli_query($con, $sql);
			if ($numrows>0)
			{
				
				?>
				<div class="table-responsive">
				  <table class="table table-hover">
					<tr  class="default">

				'.PHP_EOL);

				foreach ($this->campos as $campo)
				{
					if($campo['visible']==1)
					{
						fwrite($out, '<th>'.$campo['clave_campo'].'</th>'.PHP_EOL);
					}
				}
				fwrite($out,'
						<th><span class="pull-right">Acciones</span></th>
						
					</tr>
					<?php
					while ($row=mysqli_fetch_array($query))
					{
					'.PHP_EOL);


					foreach ($this->indice as $campo) 
					{
						if($campo['visible']==1||$campo['editable']==1||$campo['indice']==1)
						{
							fwrite($out,'$'.$campo['clave_campo'].'=$row["'.$campo['clave_campo'].'"];'.PHP_EOL);
						}
					}

					if (isset($this->indices_externos))
					{
						foreach ($this->indices_externos as $campo) 
						{
							if($campo['visible']==1||$campo['editable']==1||$campo['indice']==1)
							{
								fwrite($out,'$'.$this->clave_tabla.'_'.$campo['clave_campo'].'_'.$this->nombre_origen.'=$row["'.$this->clave_tabla.'_'.$campo['clave_campo'].'_'.$this->nombre_origen.'"];'.PHP_EOL);
							}
						}
					}

					foreach ($this->campos as $campo) 
					{
						if($campo['visible']==1||$campo['editable']==1||$campo['indice']==1)
						{
							fwrite($out,'$'.$campo['clave_campo'].'=$row["'.$campo['clave_campo'].'"];'.PHP_EOL);
						}
					}
					
					fwrite($out, '	
						?>'.PHP_EOL);
						

					foreach ($this->indice as $campo) 
					{
						if($campo['visible']==1||$campo['editable']==1||$campo['indice']==1)
						{
							fwrite($out, '<input type="hidden" value="<?php echo '.'$'.'row["'.$campo['clave_campo'].'"];?>" id="edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'<?php echo '.'$'.''.$orderby.';?>">'.PHP_EOL);
						}
					}
		/*
					if (isset($this->indices_externos))
					{
						foreach ($this->indices_externos as $campo) 
						{
							if($campo['visible']==1||$campo['editable']==1||$campo['indice']==1)
							{
								fwrite($out, '<input type="hidden" value="<?php echo '.'$'.'row["'.$this->clave_tabla.'_'.$campo['clave_campo'].'_'.$this->nombre_origen.'"];?>" id="'.$this->clave_tabla.'_'.$campo['clave_campo'].'_'.$this->nombre_origen.'<?php echo '.'$'.$this->clave_tabla.'_'.$campo['clave_campo'].'_'.$this->nombre_origen.';?>">'.PHP_EOL);
							}
						}
					}
		*/

					foreach ($this->campos as $campo)
					{
						if($campo['visible']==1||$campo['editable']==1||$campo['indice']==1)
						{
							fwrite($out, '<input type="hidden" value="<?php echo '.'$'.'row["'.$campo['clave_campo'].'"];?>" id="edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'<?php echo '.'$'.''.$orderby.';?>">'.PHP_EOL);
						}
					}

					fwrite($out, '	
						<tr>'.PHP_EOL);

					foreach($this->campos as $campo)
					{
						if($campo['visible']==1)
						{
							if($campo['tipo_campo']==6)
							{

								fwrite($out,'<td><img id="img_'.$campo['clave_campo'].'<?php echo '.'$'.$orderby.';?>" src="../'.$this->clave_tabla.'/'.$campo['carpeta'].'/<?php echo $'.$campo['clave_campo'].'; ?>" width="100"></td>'.PHP_EOL);
							}
							else
							{
								fwrite($out,'<td><?php echo $'.$campo['clave_campo'].'; ?></td>'.PHP_EOL);
							}
						}
					}
					fwrite($out,'
						<td >
							<span class="pull-right">

							');
								if($this->tipo_tabla==1)
								{
									fwrite($out, '
									<a href="ver.php?'.$this->clave_tabla.'=<?php echo $'.$orderby.';?>" class="btn btn-secondary" title="Ver '.$this->clave_tabla.'" >
									<i class="fas fa-xs fa-eye"></i>
									</a> ');
								}

								fwrite($out, '
								<a href="#" class="btn btn-secondary" title="Editar '.$this->clave_tabla.'" onclick="obtener_datos'.$this->clave_tabla.'('."'".'<?php echo $'.$orderby.';?>'."'".');" data-toggle="modal" data-target="#'.$this->clave_tabla.'_modal_edit">
								<i class="fas fa-xs fa-edit"></i>
								</a> 

								<a href="#" class="btn btn-secondary" title="Borrar '.$this->clave_tabla.'" onclick="eliminar'.$this->clave_tabla.'('."'".'<?php echo $'.$orderby.'; ?>'."'".');">
								<i class="fas fa-xs fa-trash"></i>
								</a>
							</span>
						</td>
							
						</tr>
						<?php
					}
					?>
					<tr>
						<td colspan=9>
							<span class="pull-right">
								<?php echo paginate($reload, $page, $total_pages, $adjacents); ?>	
							</span>
						</td>
					</tr>
				  </table>
				</div>
				<?php
			}
		}?>
		'.PHP_EOL);
	}

	public function script_modal($url)
	{

		$out = fopen($url.'/'.$this->clave_tabla.".js", "w+");
		fwrite($out,'		
		$(document).ready(function(){
			$("#resultados'.$this->clave_tabla.'").fadeOut(2000);
			load'.$this->clave_tabla.'(1);
		});

		function getParameterByName(name)
		{
		    if (name !== "" && name !== null && name != undefined)
		    {
		        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
		        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
		            results = regex.exec(location.search);
		        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
		    }
		    else
		    {
		        var arr = location.href.split("/");
		        return arr[arr.length - 1];
		    }
		}
		
		function load'.$this->clave_tabla.'(page)
		{');

			if($this->tipo_tabla==2)
			{
			fwrite($out, "
			var ".$this->nombre_origen." = getParameterByName('".$this->nombre_origen."');");
			}
			fwrite($out, '
			var q= $("#q").val();
			$("#loader").fadeIn("slow");
			$.ajax({
				');
			if($this->tipo_tabla==1)
			{
			fwrite($out, '
					url:"../actions/'.$this->clave_tabla.'_search.php?action=ajax&page="+page+"&q="+q,');
			}
			else
			{
			fwrite($out, '
					url:"../actions/'.$this->clave_tabla.'_search.php?action=ajax&'.$this->nombre_origen.'="+'.$this->nombre_origen.'+"&page="+page+"&q="+q,');
			}
			fwrite($out, '
					beforeSend: function(objeto)
				{
					$("#loader").html("'. "<img src='../img/ajax-loader.gif'> Cargando..." .'");
				},
				success:function(data)
				{
					$(".outer_div'.$this->clave_tabla.'").html(data).fadeIn("slow");
					$("#loader").html("");
					borrar_modales'.$this->clave_tabla.'();
					$("#'.$this->clave_tabla.'_modal_add").removeData();

					//$("#'.$this->clave_tabla.'_modal_add" ).modal("hide");
					//$("#'.$this->clave_tabla.'_modal_edit" ).modal("hide");
				}
			});
		}'.PHP_EOL);
				
		fwrite($out,'		
		function eliminar'.$this->clave_tabla.' (id)
		{
			var q= $("#q").val();
			if (confirm("Realmente deseas eliminar el '.$this->clave_tabla.'"))
			{	
				$.ajax({
					type: "GET",
					url: "../actions/'.$this->clave_tabla.'_search.php",
					data: "id="+id,"q":q,
					beforeSend: function(objeto)
					{
						$("#resultados'.$this->clave_tabla.'").html("Mensaje: Cargando...");
					},
					success: function(datos)
					{
						$("#resultados'.$this->clave_tabla.'").html(datos);
						load'.$this->clave_tabla.'(1);
					}
				});
			}
		}'.PHP_EOL);
				
				
		
		fwrite($out,'		
		$("#'.$this->clave_tabla.'_add_form" ).submit(function( event ) {
			$("#'.$this->clave_tabla.'_guardar_datos").attr("disabled", true);');
			
		if(isset($this->archivos))
		{
				fwrite($out, '
				var formData = new FormData();
	    		');
				foreach ($this->archivos as $campo)
				{
				fwrite($out, '
					formData.append("add_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_campo", add_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_campo);');
				}
				fwrite($out, '
				var objArr = [];
				');
				$cadena='';
				foreach ($this->campos as $campo)
				{
					if($campo['tipo_campo']!=6 && $campo['editable']==1)
					{
						if($campo['tipo_campo']==7)
						{
							fwrite($out, 'var_add_'.$campo['clave_campo'].'=document.getElementById("add_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_campo");');
							fwrite($out, '
								objArr.push("add_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_campo", var_add_'.$campo['clave_campo'].');');
						}
						else
						{
							fwrite($out, '
								objArr.push("add_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_campo", add_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_campo );');
						}
					}
				}

				//$cadena=substr($cadena, 0,-2);
				fwrite($out, '

				formData.append("objArr", JSON.stringify( objArr ));');
		}
		else
		{
			fwrite($out,'
		 		var formData = $(this).serialize();
		 	');
		}
			fwrite($out, '
			 $.ajax({
					type: "POST",
					url: "../actions/'.$this->clave_tabla.'_add_action.php",
					data: formData,
					beforeSend: function(objeto)
					{
						$("#'.$this->clave_tabla.'_resultados_modal_add").html("Mensaje: Cargando...");
					},
					success: function(datos)
					{
						$("#'.$this->clave_tabla.'_resultados_modal_add").html(datos);
						$("#'.$this->clave_tabla.'_guardar_datos").attr("disabled", false);
						load'.$this->clave_tabla.'(1);
				  	}
			});
			event.preventDefault();
		});'.PHP_EOL);


		fwrite($out, '
		$("#'.$this->clave_tabla.'_edit_form" ).submit(function( event ) {
			$("#'.$this->clave_tabla.'_actualizar_datos").attr("disabled", true);');
			
		if(isset($this->archivos))
		{
				fwrite($out, '
				var formData = new FormData();
	    		');
				foreach ($this->archivos as $campo)
				{
					if($campo['tipo_campo']==7)
					{
						fwrite($out, 'var_edit_'.$campo['clave_campo'].'=document.getElementById("edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_campo");');
						$cadena.='"edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_campo", var_edit_'.$campo['clave_campo'].', ';
					}
					else
					{
				fwrite($out, '
					formData.append("edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_campo", edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_campo);');
					}
				}
				fwrite($out, '
				var objArr = [];
				');
				$cadena='';
				foreach ($this->campos as $campo)
				{
					if($campo['tipo_campo']!=6 && $campo['editable']==1)
					{
						if($campo['tipo_campo']==7)
						{
							fwrite($out, 'var_edit_'.$campo['clave_campo'].'=document.getElementById("edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_campo");');
							
							fwrite($out, '
								objArr.push("edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_campo", var_edit_'.$campo['clave_campo'].');');
						}
						else
						{
							
							fwrite($out, '
								objArr.push("edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_campo", edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_campo);');
						}
					}
				}
				//$cadena=substr($cadena, 0,-2);
				fwrite($out, '
				formData.append("objArr", JSON.stringify( objArr ));');
		}
		else
		{
			fwrite($out,'
		 		var formData = $(this).serialize();
		 	');
		}



			fwrite($out, '
			$.ajax({
				type: "POST",
				url: "../actions/'.$this->clave_tabla.'_edit_action.php",
				data: formData,
				beforeSend: function(objeto)
				{
					$("#'.$this->clave_tabla.'_resultados_modal_edit").html("Mensaje: Cargando...");
				},
				success: function(datos)
				{
					$("#'.$this->clave_tabla.'_resultados_modal_edit").html(datos);
					load'.$this->clave_tabla.'(1);
				}
			});
			event.preventDefault();
		});'.PHP_EOL);
		

		fwrite($out,'
		function obtener_datos'.$this->clave_tabla.'(id)
		{
		');
		foreach ($this->indice as $campo)
		{
			fwrite($out,'
						$("#edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_indice'.'").val("");
						$("#edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_indice'.'").val(id);'.PHP_EOL);
		}

		if (isset($this->indices_externos)) 
		{

			if($this->tipo_tabla==2)
			{
			fwrite($out, "
			var ".$this->nombre_origen." = getParameterByName('".$this->nombre_origen."');");
			}
			foreach ($this->indices_externos as $campo)
			{
				fwrite($out,'
				$("#edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_'.$this->nombre_origen.'_externo'.'").val("");');
				fwrite($out,'
				$("#edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_'.$this->nombre_origen.'_externo'.'").val('.$this->nombre_origen.');'.PHP_EOL);
			}
		}
		$script='';
		foreach ($this->campos as $campo)
		{
			if($campo['tipo_campo']==6)
			{

				fwrite($out,'
				$("#edit_img_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_campo").attr("src",$("#img_'.$campo['clave_campo'].'"+id).attr("src"));
				//uno'.$campo['clave_campo']."();");

				$script.= "
				function uno".$campo['clave_campo']."() {
					$('#edit_".$this->clave_tabla.'_'.$campo['clave_campo']."').change(function(e) {
						addImage(e); 
					});

					function addImage(e){
						var file = e.target.files[0],
						imageType = /image.*/;

						if (!file.type.match(imageType))
						return;

						var reader = new FileReader();
						reader.onload = fileOnload;
						reader.readAsDataURL(file);
					}

					function fileOnload(e) {
						var result=e.target.result;
						$('#img_".$campo['clave_campo']."').attr('src',result);
					}
				}";
			}
			else
			{

				fwrite($out,'
					$("#edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_campo").val("");
					var '.$campo['clave_campo'].' = $("#edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'"+id).val();
					$("#edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_campo").val('.$campo['clave_campo'].');'.PHP_EOL);
			}
		}
		fwrite($out,'
		}');


		//fwrite($out,$script.PHP_EOL);



		fwrite($out,'

		function borrar_modales'.$this->clave_tabla.'()
		{');

		foreach ($this->campos as $campo) 
		{
			fwrite($out,'
			$("#edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'").val("");
			$("#add_'.$this->clave_tabla.'_'.$campo['clave_campo'].'").val("");');
		}



		foreach ($this->indice as $campo)
		{
			fwrite($out,'
			$("#edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'").val("");
			$("#add_'.$this->clave_tabla.'_'.$campo['clave_campo'].'").val("");');
		}

		if (isset($this->indices_externos)) 
		{
			foreach ($this->indices_externos as $campo)
			{
				fwrite($out,'
				$("#edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_'.$this->clave_tabla.'").val("");
				$("#add_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_'.$this->clave_tabla.'").val("");
				');
			}
		}
			fwrite($out,'
			}');
			fclose($out);
	}

	public function modal_add($url)
	{
		$out = fopen($url.'/'.$this->clave_tabla."_add.php", "w+");
		fwrite($out, '<div class="modal fade" id="'.$this->clave_tabla.'_modal_add" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title" id="'.$this->clave_tabla.'_modal_addLabel"><i class="');
									fwrite($out, $this->icono($this->id_icono));

						fwrite($out,'"></i> NUEVO '.$this->clave_tabla.'</h4>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<div class="modal-body">
						<form class="form-horizontal" data-async data-target="#rating-modal" method="post" id="'.$this->clave_tabla.'_add_form" name="'.$this->clave_tabla.'_add_form" '.(($this->id_archivo==1)?'action="../actions/'.$this->clave_tabla.'_add_action.php" enctype="multipart/form-data"':"").'>
							<div id="'.$this->clave_tabla.'_resultados_modal_add"></div>'.PHP_EOL);

							if(isset($this->indices_externos))
							{
								foreach ($this->indices_externos as $campo)
								{ 
									if($campo['indice']==1)
									{
										fwrite($out,'<div class="form-group">
											<input type="hidden" class="form-control" id="add_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_'.$this->nombre_origen.'_externo'.'" name="add_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_'.$this->nombre_origen.'_externo'.'" placeholder="'.$campo['descripcion_campo'].'"');
											if($campo['requerido']==1)
												fwrite($out,' required');
													fwrite($out, '	value="<?php echo $_GET["'.$this->nombre_origen.'"];?>">
										</div>');
									}
								}
							}
							foreach ($this->campos as $campo)
							{
								if( $campo['editable']==1 && $campo['indice']!=1 )
								{
									switch ($campo['tipo_campo'])
									{
										case 1://INTEGER
											fwrite($out,'									<div class="form-group">
													<label for="for_'.$campo['clave_campo'].'" class="control-label">'.$campo['clave_campo'].'</label>
													<input type="number" value="" maxlength="10" class="form-control" id="add_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_campo'.'" name="add_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_campo'.'" placeholder="'.$campo['descripcion_campo'].'"');
											break;

										case 2://VARCHAR
											fwrite($out,'									<div class="form-group">
													<label for="for_'.$campo['clave_campo'].'" class="control-label">'.$campo['clave_campo'].'</label>
													<input type="text" class="form-control" id="add_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_campo'.'" name="add_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_campo'.'" placeholder="'.$campo['descripcion_campo'].'"');
											break;

										case 3://DATE
											fwrite($out,'									<div class="form-group">
													<label for="for_'.$campo['clave_campo'].'" class="control-label">'.$campo['clave_campo'].'</label>
													<input type="text" data-toggle="datepicker" class="form-control" id="add_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_campo'.'" name="add_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_campo'.'" placeholder="'.$campo['descripcion_campo'].'"');
											break;

										case 4://DATETIME
											fwrite($out,'									<div class="form-group">
													<label for="for_'.$campo['clave_campo'].'" class="control-label">'.$campo['clave_campo'].'</label>
													<input type="text" data-toggle="datepicker" class="form-control" id="add_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_campo'.'" name="add_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_campo'.'" placeholder="'.$campo['descripcion_campo'].'"');
											break;

										case 5://TEXT
											fwrite($out,'									<div class="form-group">
													<label for="for_'.$campo['clave_campo'].'" class="control-label">'.$campo['clave_campo'].'</label>
													<input type="text" class="form-control" id="add_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_campo'.'" name="add_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_campo'.'" placeholder="'.$campo['descripcion_campo'].'"');
											break;

										case 6://FILE type="file" class="form-control" name="image" value=""
											fwrite($out,'									<div class="form-group">
													<label for="for_'.$campo['clave_campo'].'" class="control-label">'.$campo['clave_campo'].'</label>

	                        						<img id="img_'.$campo['clave_campo'].'"src="<?php echo "../'.$campo['carpeta'].'/$'.$campo['clave_campo'].'"; ?>" width="100">
													<input type="file" class="form-control" id="add_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_campo'.'" name="add_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_campo'.'" value="" accept="'.$this->formatos_permitidos_sin_comillas($campo['jpeg'] ,$campo['png'] ,$campo['gif'] ,$campo['tif'] ,$campo['svg'] ,$campo['eps'] ,$campo['pdf'] ,$campo['xlsx'],$campo['xlsm'], $campo['doc'] ,$campo['docx'],$campo['dwg'] ,$campo['txt'] ,$campo['zip'] ,$campo['rar']).'"');
											break;

										case 7://BOOLEAN
											fwrite($out,'									<div class="form-group">
													<label for="for_'.$campo['clave_campo'].'" class="control-label">'.$campo['clave_campo'].'</label>
													<input type="checkbox" class="form-control" id="add_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_campo'.'" name="add_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_campo'.'" placeholder="'.$campo['descripcion_campo'].'"');
											break;

										case 8://DOUBLE
											fwrite($out,'									<div class="form-group">
													<label for="for_'.$campo['clave_campo'].'" class="control-label">'.$campo['clave_campo'].'</label>
													<input type="number" value="" maxlength="10" step="0.0000001" pattern="^[0-9]{1,5}(\.[0-9]{0,2})?$" class="form-control" id="add_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_campo'.'" name="add_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_campo'.'" placeholder="'.$campo['descripcion_campo'].'" ');
											break;

										default:
											# code...
											break;
									}
									if($campo['requerido']==1)
									{
										fwrite($out,' required');
									}
									fwrite($out, ' >'.PHP_EOL);
									fwrite($out, '									</div>'.PHP_EOL);
								}
							}
							fwrite($out, '									<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
								<button type="submit" class="btn btn-primary" id="'.$this->clave_tabla.'_guardar_datos">Guardar datos</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>');
		
			fclose($out);
	}

	public function modal_edit($url)
	{
		$out = fopen($url.'/'.$this->clave_tabla."_edit.php", "w+");
		$nombre=$this->clave_tabla;
		fwrite($out, '<div class="modal fade" id="'.$this->clave_tabla.'_modal_edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title" id="myModalLabel"><i class="');
									
										fwrite($out, $this->icono($this->id_icono));


						fwrite($out,'"></i> EDITAR '.$this->clave_tabla.'</h4>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<div class="modal-body">
						<form class="form-horizontal" data-async data-target="#rating-modal" method="post" id="'.$this->clave_tabla.'_edit_form" name="'.$this->clave_tabla.'_edit_form" '.(($this->id_archivo==1)?'action="../actions/'.$this->clave_tabla.'_edit_action.php" enctype="multipart/form-data"':"").'>
							<div id="'.$this->clave_tabla.'_resultados_modal_edit"></div>');

							foreach ($this->indice as $campo)
							{ 
									fwrite($out,'<div class="form-group">
										<input type="hidden" class="form-control" id="edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_indice'.'" name="edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_indice'.'" placeholder="'.$campo['descripcion_campo'].'" value="" ');
										if($campo['requerido']==1)
											fwrite($out,' required');
												fwrite($out, '	>
									</div>');
							}

							if(isset($this->indices_externos))
							{
								foreach ($this->indices_externos as $campo)
								{ 
									if($campo['indice']==1)
									{
										fwrite($out,'<div class="form-group">
											<input type="hidden" class="form-control" id="edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_'.$this->nombre_origen.'_externo'.'" name="edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_'.$this->nombre_origen.'_externo'.'" placeholder="'.$campo['descripcion_campo'].'" value="" ');
											if($campo['requerido']==1)
												fwrite($out,' required');
													fwrite($out, '	>
										</div>');
									}
								}
							}
							
							foreach ($this->campos as $campo)
							{ 
								if( $campo['editable']==1 && $campo['indice']!=1 )
								{
									switch ($campo['tipo_campo'])
									{
										case 1://INTEGER
											fwrite($out,'									<div class="form-group">
													<label for="for_'.$campo['clave_campo'].'" class="control-label">'.$campo['clave_campo'].'</label>
													<input type="number" value="" maxlength="10" class="form-control" id="edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_campo'.'" name="edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_campo'.'" placeholder="'.$campo['descripcion_campo'].'" value=""');
											break;

										case 2://VARCHAR
											fwrite($out,'									<div class="form-group">
													<label for="for_'.$campo['clave_campo'].'_campo'.'" class="control-label">'.$campo['clave_campo'].'</label>
													<input type="text" class="form-control" id="edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_campo'.'" name="edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_campo'.'" placeholder="'.$campo['descripcion_campo'].'" value=""');
											break;

										case 3://DATE
											fwrite($out,'									<div class="form-group">
													<label for="for_'.$campo['clave_campo'].'_campo'.'" class="control-label">'.$campo['clave_campo'].'</label>
													<input type="text" data-toggle="datepicker" class="form-control" id="edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_campo'.'" name="edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_campo'.'" placeholder="'.$campo['descripcion_campo'].'" value=""');
											break;

										case 4://DATETIME
											fwrite($out,'									<div class="form-group">
													<label for="for_'.$campo['clave_campo'].'_campo'.'" class="control-label">'.$campo['clave_campo'].'</label>
													<input type="text" data-toggle="datepicker" class="form-control" id="edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_campo'.'" name="edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_campo'.'" placeholder="'.$campo['descripcion_campo'].'" value=""');
											break;

										case 5://TEXT
											fwrite($out,'									<div class="form-group">
													<label for="for_'.$campo['clave_campo'].'_campo'.'" class="control-label">'.$campo['clave_campo'].'</label>
													<input type="text" class="form-control" id="edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_campo'.'" name="edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_campo'.'" placeholder="'.$campo['descripcion_campo'].'" value=""');
											break;

										case 6://FILE type="file" class="form-control" name="image" value=""
											fwrite($out,'									<div class="form-group">
													<label for="for_'.$campo['clave_campo'].'_campo'.'" class="control-label">'.$campo['clave_campo'].'</label>

	                        						<img id="edit_img_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_campo'.'"src="<?php echo "../'.$campo['carpeta'].'/$'.$campo['clave_campo'].'"; ?>" width="100">
													<input type="file" multiple="multiple" class="form-control" id="edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_campo'.'" name="edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_campo'.'" value="" accept="'.$this->formatos_permitidos_sin_comillas($campo['jpeg'] ,$campo['png'] ,$campo['gif'] ,$campo['tif'] ,$campo['svg'] ,$campo['eps'] ,$campo['pdf'] ,$campo['xlsx'],$campo['xlsm'], $campo['doc'] ,$campo['docx'],$campo['dwg'] ,$campo['txt'] ,$campo['zip'] ,$campo['rar']).'" value=""');

											break;

										case 7://BOOLEAN
											fwrite($out,'									<div class="form-group">
													<label for="for_'.$campo['clave_campo'].'_campo'.'" class="control-label">'.$campo['clave_campo'].'</label>
													<input type="checkbox" class="form-control" id="edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_campo'.'" name="edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_campo'.'" placeholder="'.$campo['descripcion_campo'].'" value=""');
											break;

										case 8://DOUBLE
											fwrite($out,'									<div class="form-group">
													<label for="for_'.$campo['clave_campo'].'_campo'.'" class="control-label">'.$campo['clave_campo'].'</label>
													<input type="number" value="" maxlength="10" step="0.0000001" pattern="^[0-9]{1,5}(\.[0-9]{0,2})?$" class="form-control" id="edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'_campo'.'" name="edit_'.$this->clave_tabla.'_'.$campo['clave_campo'].'" placeholder="'.$campo['descripcion_campo'].'" value=""');
											break;

										default:
											# code...
											break;
									}
									if($campo['requerido']==1)
									{
										fwrite($out,' required');
									}
									fwrite($out, ' >'.PHP_EOL);
									fwrite($out, '									</div>'.PHP_EOL);
								}
							}
						
							fwrite($out, '<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
								<button type="submit" class="btn btn-primary" id="'.$this->clave_tabla.'_actualizar_datos">Actualizar datos</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>');
		fclose($out);
	}

}//fin de la clase
?>