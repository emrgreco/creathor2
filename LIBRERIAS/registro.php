<?php
	
	require 'USUARIO_CLASS.php';
	
	$errors = array();
	$message = array();
	if(!empty($_POST))
	{


		$nombre = mysqli_real_escape_string($con,(strip_tags($_POST['nombre'],ENT_QUOTES)));
		$appater=mysqli_real_escape_string($con,(strip_tags($_POST['appater'],ENT_QUOTES)));
		$apmater=mysqli_real_escape_string($con,(strip_tags($_POST['apmater'],ENT_QUOTES)));
		$password = mysqli_real_escape_string($con,(strip_tags($_POST['password'],ENT_QUOTES)));
		$con_password = mysqli_real_escape_string($con,(strip_tags($_POST['con_password'],ENT_QUOTES)));
		$email = mysqli_real_escape_string($con,(strip_tags($_POST['email'],ENT_QUOTES)));
		$captcha = mysqli_real_escape_string($con,(strip_tags($_POST['g-recaptcha-response'],ENT_QUOTES)));
		
		$activo = 0;
		$secret = '6LdO8F0UAAAAAMjXB5lcWeupvdR04BhqGB8nTorf';//Modificar
		//$secret = '6LdvJl4UAAAAAF1OYitKXIjXnrZKrbwVc0nkPPap';//Modificar
		
		if(!$captcha){
			$errors[] = "Por favor verifica el captcha";
		}
		
		if(isNull($nombre, $password, $con_password, $email))
		{
			$errors[] = "Debe llenar todos los campos";
		}
		
		if(!isEmail($email))
		{
			$errors[] = "Dirección de correo inválida";
		}
		
		if(!validaPassword($password, $con_password))
		{
			$errors[] = "Las contraseñas no coinciden";
		}
		if(emailExiste($email))
		{
			$errors[] = "El correo electronico $email ya existe";
		}
		
		if(count($errors) == 0)
		{
			$response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=$secret&response=$captcha");
			
			$arr = json_decode($response, TRUE);
			
			if($arr['success'])
			{
				
				$pass_hash = hashPassword($password);
				$token = generateToken();
				
				$registro = registraUsuario($pass_hash, $nombre, $appater, $apmater, $email, $activo, $token);
				
				if($registro > 0 )
				{
					
					$url = 'http://'.$_SERVER["SERVER_NAME"].'/USUARIOS/activar.php?id='.$registro.'&val='.$token;
					
					$asunto = 'Activar Cuenta';
					$cuerpo = "
					<html>
					<body>
						<table width='100%' bgcolor='#e0e0e0' cellpadding='0' cellspacing='0' border='0'>
							<tr>
								<td>
									<table align='center' width='100%' border='0' cellpadding='0' cellspacing='0' style='max-width:650px; background-color:#fff; font-family:Verdana, Geneva, sans-serif;'>
										<thead>

											<tr height='80'>
												<th colspan='4' style='background-color:#FFBE4A; border-bottom:solid 1px #bdbdbd; font-family:Verdana, Geneva, sans-serif; color:#333; font-size:34px;'></th>
											</tr>
											<tr height='80'>
												<th colspan='4' style='background-color:#f5f5f5; border-bottom:solid 1px #bdbdbd; font-family:Verdana, Geneva, sans-serif; color:#333; font-size:34px;' >Zwanzing und Neunzehn</th>
											</tr>
								        </thead>
								        <tbody>
											<tr align='center' height='40' style='border-bottom:solid 1px #bdbdbd; font-size:25px; font-family:Verdana, Geneva, sans-serif;'>
												<td text-align:center;'>Recuperar Contraseña</td>
											</tr>
											<tr>
												<td colspan='4' style='padding:15px;'>
												<p style='font-size:20px;'>Estimado $nombre: <br /><br />Para continuar con el proceso de recuperacion de contraseña, es indispensable de click en la siguiente liga: </p>
												<br>
												<center><a href='$url'>Activar Cuenta</a></center>
												</td>
											</tr>
											<tr height='20'>
												<th colspan='4' style='background-color:#FFBE4A; border-bottom:solid 1px #bdbdbd; font-family:Verdana, Geneva, sans-serif; color:#333; font-size:34px;'></th>
											</tr>
							        	</tbody>
							        </table>
						    	</td>
					    	</tr>
					    </table>
				    </body>
				    </html>";
					if(enviarEmail($email, $nombre, $asunto, $cuerpo))
					{
						header('Location: http://'.$_SERVER["SERVER_NAME"].'/USUARIOS/index.php?email="'.$email.'"');
					}
					else 
					{
						$errors[] = "Error al enviar Email";
					}
				}
				else
				{
					$errors[] = "Error al Registrar";
				}
			}
			else
			{
				$errors[] = 'Error al comprobar Captcha';
			}
			
		}
		
	}
	
?>
<html>
	<head>
		<title>Registro</title>
		
		<link rel="stylesheet" href="css/bootstrap.min.css" >
		<link rel="stylesheet" href="css/bootstrap-theme.min.css" >
		<script src="js/bootstrap.min.js" ></script>
		<script src='https://www.google.com/recaptcha/api.js'></script>
	</head>
	
	<body>
		<div class="container">
			<div id="signupbox" style="margin-top:50px" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
				<div class="panel panel-info">
					<div class="panel-heading">
						<div class="panel-title">Reg&iacute;strate</div>
						<div style="float:right; font-size: 85%; position: relative; top:-10px"><a id="signinlink" href="index.php">Iniciar Sesi&oacute;n</a></div>
					</div>  
					
					<div class="panel-body" >
						
						<form id="signupform" class="form-horizontal" role="form" action="<?php $_SERVER['PHP_SELF'] ?>" method="POST" autocomplete="off">
							
							<div id="signupalert" style="display:none" class="alert alert-danger">
								<p>Error:</p>
								<span></span>
							</div>
							
							<div class="form-group">
								<label for="email" class="col-md-3 control-label">Email</label>
								<div class="col-md-9">
									<input type="email" class="form-control" name="email" placeholder="Email" value="<?php if(isset($email)) echo $email; ?>" required>
								</div>
							</div>

							<div class="form-group">
								<label for="nombre" class="col-md-3 control-label">Nombre:</label>
								<div class="col-md-9">
									<input type="text" class="form-control" name="nombre" placeholder="Nombre" value="<?php if(isset($nombre)) echo $nombre; ?>" required >
								</div>
							</div>
							
							<div class="form-group">
								<label for="appater" class="col-md-3 control-label">Apellido Paterno:</label>
								<div class="col-md-9">
									<input type="text" class="form-control" name="appater" placeholder="Apellido Paterno" value="<?php if(isset($appater)) echo $appater; ?>" required >
								</div>
							</div>
							
							<div class="form-group">
								<label for="apmater" class="col-md-3 control-label">Apellido Materno:</label>
								<div class="col-md-9">
									<input type="text" class="form-control" name="apmater" placeholder="Apellido Materno" value="<?php if(isset($apmater)) echo $apmater; ?>" required >
								</div>
							</div>
							
							<div class="form-group">
								<label for="password" class="col-md-3 control-label">Password</label>
								<div class="col-md-9">
									<input type="password" class="form-control" name="password" placeholder="Password" required>
								</div>
							</div>
							
							<div class="form-group">
								<label for="con_password" class="col-md-3 control-label">Confirmar Password</label>
								<div class="col-md-9">
									<input type="password" class="form-control" name="con_password" placeholder="Confirmar Password" required>
								</div>
							</div>
							
							
							<div class="form-group">
								<center>
									<label for="captcha" class="col-md-3 control-label"></label>
									<!--div class="g-recaptcha" data-sitekey="6LdvJl4UAAAAAD8zEZHU304mBOvmtFw-s1--dbkV"></div-->
									<div class="g-recaptcha" data-sitekey="6LdO8F0UAAAAALDkhuzLqivYxwR7QwbR_U2sN4ZI"></div>
								</center>
							</div>
							
							<div class="form-group">                                      
								<div class="col-md-offset-3 col-md-9">
									<button id="btn-signup" type="submit" class="btn btn-info"><i class="icon-hand-right"></i>Registrar</button> 
								</div>
							</div>
						</form>
						<?php echo resultBlock($errors); ?>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>															