<?php
	
	require 'USUARIO_CLASS.php';
	$message=array();
	$user_id =mysqli_real_escape_string($con,(strip_tags($_POST['user_id'],ENT_QUOTES)));
	$token =mysqli_real_escape_string($con,(strip_tags($_POST['token'],ENT_QUOTES)));
	$password =mysqli_real_escape_string($con,(strip_tags($_POST['password'],ENT_QUOTES)));
	$con_password =mysqli_real_escape_string($con,(strip_tags($_POST['con_password'],ENT_QUOTES)));
	
	if(validaPassword($password, $con_password))
	{
		$pass_hash = hashPassword($password);
		if(cambiaPassword($pass_hash, $user_id, $token))
		{
			$message[]= "Contrase&ntilde;a Modificada ";
		}
		else
		{
			$message[]= "Error al modificar contrase&ntilde;a";	
		}
	}
	else
	{	
		$message[]= 'Las contraseñas no coinciden';	
	}
	header('Location: http://'.$_SERVER["SERVER_NAME"].'/ModuloUsuarios/index.php');
	
	
?>	