<?php
	require_once 'config/db.php';
	require_once 'config/conexion.php';


	//PERMISOS
	/**
		Guarda los valores por defecto de los permisos
	*/
	$permisos= array(
		'usuario' => 0,
		'master' => 0 
	);

	function isNull($nombre, $pass, $pass_con, $email)
	{
		if(strlen(trim($nombre)) < 1 || strlen(trim($pass)) < 1 || strlen(trim($pass_con)) < 1 || strlen(trim($email)) < 1)
		{
			return true;
		}
		else
		{
			return false;
		}		
	}
	
	function isEmail($email)
	{
		if (filter_var($email, FILTER_VALIDATE_EMAIL))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	function validaPassword($var1, $var2)
	{
		if (strcmp($var1, $var2) !== 0)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	
	function minMax($min, $max, $valor)
	{
		if(strlen(trim($valor)) < $min)
		{
			return true;
		}
		else if(strlen(trim($valor)) > $max)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	function emailExiste($email)
	{
		global $con;
		$query1=mysqli_query($con, "SELECT id FROM usuarios WHERE correo = '$email' LIMIT 1");
		$row_query1=mysqli_fetch_array($query1);
		
		if(isset($row_query1))
		{
			return true;
		}
		else
		{
			return false;	
		}
	}
	
	function generateToken()
	{
		$gen = md5(uniqid(mt_rand(), false));	
		return $gen;
	}
	
	function hashPassword($password) 
	{
		$hash = password_hash($password, PASSWORD_DEFAULT);
		return $hash;
	}
	
	function resultBlock($errors, $tipo='errors')
	{
		if($tipo=='errors')
		{
			if(count($errors) > 0)
			{
				echo "<div id='error' class='alert alert-danger' role='alert'>
				<a href='#' onclick=\"showHide('error');\">[X]</a>
				<ul>";
				foreach($errors as $error)
				{
					echo "<li>".$error."</li>";
				}
				echo "</ul>";
				echo "</div>";
			}
		}
		else if($tipo=='message')
		{
			if(count($errors) > 0)
			{
				echo "<div id='message' class='alert alert-success' role='alert'>
				<a href='#' onclick=\"showHide('message');\">[X]</a>
				<ul>";
				foreach($errors as $error)
				{
					echo "<li>".$error."</li>";
				}
				echo "</ul>";
				echo "</div>";
			}
		}
	}
	
	function registraUsuario($pass_hash, $nombre, $appater, $apmater, $email, $activo, $token)
	{
		
		global $con;

		$pass_hash=mysqli_real_escape_string($con,(strip_tags($pass_hash,ENT_QUOTES)));
		$nombre=mysqli_real_escape_string($con,(strip_tags($nombre,ENT_QUOTES)));
		$appater=mysqli_real_escape_string($con,(strip_tags($appater,ENT_QUOTES)));
		$apmater=mysqli_real_escape_string($con,(strip_tags($apmater,ENT_QUOTES)));
		$email=mysqli_real_escape_string($con,(strip_tags($email,ENT_QUOTES)));
		$activo=intval($activo);
		$token=mysqli_real_escape_string($con,(strip_tags($token,ENT_QUOTES)));

		$query1=mysqli_query($con, "INSERT INTO usuarios (password, nombre, appater, apmater, correo, activacion, token) VALUES('$pass_hash', '$nombre', '$appater', '$apmater', '$email', $activo, '$token');");
		$id=mysqli_insert_id($con);
		if($id!=0)
		{
			return $id;	
		}
		else
		{
			return 0;
		}	
	}
	
	function enviarEmail($email, $nombre, $asunto, $cuerpo)
	{
		
		require_once 'PHPMailer/PHPMailerAutoload.php';
		
		$mail = new PHPMailer();
		$mail->isSMTP();
		$mail->SMTPAuth = true;
		$mail->SMTPSecure = TIPOSEGURIDAD; //Modificar
		$mail->Host = DOMINIO; //Modificar
		$mail->Port = PUERTO; //Modificar
		
		$mail->Username = CORREOEMISOR; //Modificar
		$mail->Password = PASSWORD; //Modificar
		
		$mail->setFrom(CORREOEMISOR, NOMBREEMISOR); //Modificar
		$mail->addAddress($email, $nombre);
		
		$mail->Subject = $asunto;
		$mail->Body    = $cuerpo;
		$mail->IsHTML(true);
		
		if($mail->send())
		return true;
		else
		return false;
	}
	
	function validaIdToken($id, $token)
	{
		global $con;
		$query1=mysqli_query($con, "SELECT activacion FROM usuarios WHERE id = $id AND token = '$token' LIMIT 1;");
		$row_query1=mysqli_fetch_array($query1);
		
		if(isset($row_query1))
		{
			if($row_query1['activacion'] == 1)
			{
				$msg = "La cuenta ya se activo anteriormente.";
			} 
			else
			{
				if(activarUsuario($id))
				{
					$msg = 'Cuenta activada.';
				}
				else
				{
					$msg = 'Error al Activar Cuenta';
				}
			}
		}
		else
		{
			$msg = 'No existe el registro para activar.';
		}
		return $msg;
	}
	
	function activarUsuario($id)
	{
		global $con;
		$query1=mysqli_query($con, "UPDATE usuarios SET activacion=1 WHERE id = $id;");
		return $query1;
	}
	
	function isNullLogin($usuario, $password)
	{
		if(strlen(trim($usuario)) < 1 || strlen(trim($password)) < 1)
		{
			return true;
		}
		else
		{
			return false;
		}		
	}

	function getRealIP()
	{
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if(getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if(getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if(getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if(getenv('HTTP_FORWARDED'))
           $ipaddress = getenv('HTTP_FORWARDED');
        else if(getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
        	$ipaddress=getHostByName(php_uname('n'));

        $ipaddress=getHostByName(php_uname('n'));
        return $ipaddress;
	}

	function login($usuario, $password)
	{
		global $con;
		$query1=mysqli_query($con, "SELECT id, password FROM usuarios WHERE correo = '$usuario' LIMIT 1;");
		$row_query1=mysqli_fetch_array($query1);
		
		if(isset($row_query1))
		{
			if(isActivo($usuario))
			{		
				$validaPassw = password_verify($password, $row_query1['password']);
				if($validaPassw)
				{
					
					$ip=getRealIP();
					$sql="INSERT INTO lugares (id, id_session, ip, fecha) VALUES (NULL, '".$row_query1['id']."','$ip',NOW());";
					$insert=mysqli_query($con, $sql);
					$_SESSION['id_usuario'] = $row_query1['id'];
					$_SESSION['tiempo'] = time();
					header("location: welcome.php");
					//echo $sql;
				} 
				else
				{
					$errors = "El nombre de usuario o correo electr&oacute;nico es incorrecta";
				}
			}
			else
			{
				$errors = 'El usuario no esta activo';
			}
		}
		else
		{
			$errors = "El nombre de usuario o correo electr&oacute;nico no existe";
		}
		return $errors;
	}
	
	function isActivo($usuario)
	{
		global $con;
		$query1=mysqli_query($con, "SELECT activacion FROM usuarios WHERE correo = '$usuario' LIMIT 1;");
		$row_query1=mysqli_fetch_array($query1);
		
		if($row_query1['activacion']==1)
		{
			return true;
		}
		else
		{
			return false;	
		}
	}	
	
	function generaTokenPass($user_id)
	{
		global $con;
		$token = generateToken();
		$sql="UPDATE usuarios SET token_password='$token', password_request=1 WHERE id = $user_id";
		$query1=mysqli_query($con, $sql);
		return $token;
	}
	
	function getValor($campo, $campoWhere, $valor)
	{
		global $con;
		$query1=mysqli_query($con, "SELECT $campo FROM usuarios WHERE $campoWhere = '$valor' LIMIT 1;");
		$row_query1=mysqli_fetch_array($query1);
		
		if(isset($row_query1))
		{
			return $row_query1[0];
		}
		else
		{
			return null;	
		}
	}
	
	function getPasswordRequest($id)
	{
		global $con;
		$query1=mysqli_query($con, "SELECT password_request FROM usuarios WHERE id = $id;");
		$row_query1=mysqli_fetch_array($query1);
		if ($row_query1['password_request'] == 1)
		{
			return true;
		}
		else
		{
			return null;	
		}
	}
	
	function verificaTokenPass($user_id, $token)
	{
		global $con;
		$query1=mysqli_query($con, "SELECT activacion FROM usuarios WHERE id = $user_id AND token_password = '$token' AND password_request = 1 LIMIT 1");
		$row_query1=mysqli_fetch_array($query1);
		if (isset($row_query1))
		{
			if($row_query1['activacion'] == 1)
			{
				return true;
			}
			else 
			{
				return false;
			}
		}
		else
		{
			return false;	
		}
	}
	
	function cambiaPassword($password, $user_id, $token)
	{
		
		global $con;
		$query1=mysqli_query($con, "UPDATE usuarios SET password = '$password', token_password='', password_request=0 WHERE id = $user_id AND token_password = '$token';");
		if($query1)
		{
			return true;
		}
		else
		{
			return false;		
		}
	}


?>