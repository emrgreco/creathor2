<<?php

require_once './config/db.php';
require_once './config/conexion.php';

global $con;

class sitio 
{
	public $usuarios;


	public $clave_sitio;
	public $id_sitio;
	public $tablas;
	function __construct($id)
	{
		$url='./NUEVO/';
		$sql ="SELECT * FROM sitio where id_sitio=".$id;
		$sql_result=mysqli_query($GLOBALS['con'], $sql);
		$row_result=mysqli_fetch_assoc($sql_result);
		$this->clave_sitio=$row_result['clave_sitio'];
		$this->id_sitio=$row_result['id_sitio'];
		$this->usuarios=$row_result['usuarios'];

		$sql="SELECT * FROM tabla WHERE id_sitio=".$id." ORDER BY tipo_tabla ASC";
		$sql_result=mysqli_query($GLOBALS['con'], $sql);
		while( $row_result=mysqli_fetch_assoc($sql_result) )
		{
			$this->tablas[] = new tabla($row_result);
		}

		$this->quematodoyhuye($url);
		$this->nuevacarpeta($url,false);
		$this->nuevacarpeta($url.'actions/',true);
		$this->nuevacarpeta($url.'modal/',true);
		$this->nuevacarpeta($url.'config/',true);
		$this->smartCopy('pagination.php', $url.'actions/');

		$this->smartCopy('LIBRERIAS/js', $url);//aquí le indicas el original y el nombre de la copia
		$this->smartCopy('LIBRERIAS/css', $url);//aquí le indicas el original y el nombre de la copia
		$this->smartCopy('LIBRERIAS/img', $url);//aquí le indicas el original y el nombre de la copia

		if($this->usuarios==1)
		{
			$this->smartCopy('LIBRERIAS/activar.php', $url);
			$this->smartCopy('LIBRERIAS/cambia_pass.php', $url);
			$this->smartCopy('LIBRERIAS/guarda_pass.php', $url);
			$this->smartCopy('LIBRERIAS/index.php', $url);
			$this->smartCopy('LIBRERIAS/logout.php', $url);
			$this->smartCopy('LIBRERIAS/recupera.php', $url);
			$this->smartCopy('LIBRERIAS/registro.php', $url);
			$this->smartCopy('LIBRERIAS/sesiones.php', $url);
			$this->smartCopy('LIBRERIAS/logout.php', $url);
			$this->smartCopy('LIBRERIAS/USUARIO_CLASS.php', $url);
			$this->smartCopy('LIBRERIAS/welcome.php', $url);

			$this->smartCopy('LIBRERIAS/PHPMailer', $url);//aquí le indicas el original y el nombre de la copia
		}

		foreach ( $this->tablas as $tabla )
		{
			if($tabla->tipo_tabla<3)
			{
				$this->nuevacarpeta($url.$tabla->clave_tabla,false);
				if(isset($tabla->archivos))
				{
					foreach ($tabla->archivos as $campo)
					{
						$this->nuevacarpeta($url.$tabla->clave_tabla.'/'.$campo['carpeta'],true);
					}
				}

				$tabla->info();
				$tabla->action_search($url.'/actions');
				$tabla->action_add_modal($url.'/actions');
				$tabla->action_edit_modal($url.'/actions');
				$tabla->script_modal($url.'/js');
				$tabla->modal_edit($url.'/modal');
				$tabla->modal_add($url.'/modal');
				$this->index_modal_principal($url.$tabla->clave_tabla, $tabla);
				if($tabla->tipo_tabla==1)
				{
					$this->index_Ver($url.$tabla->clave_tabla, $tabla);
				}
			}
		}
		$this->msqli_db_crear_conexion($url.'config/');
		$this->templates($url);
	}

	public function smartCopy($source, $dest, $options=array('folderPermission'=>0755,'filePermission'=>0755))
    {
        $result=false;
       $dirsource="";
        if (is_file($source)) {
            if ($dest[strlen($dest)-1]=='/') {
                if (!file_exists($dest)) {
                    cmfcDirectory::makeAll($dest,$options['folderPermission'],true);
                }
                $__dest=$dest."/".basename($source);
            } else {
                $__dest=$dest;
            }
            $result=copy($source, $__dest);
            chmod($__dest,$options['filePermission']);
           
        } elseif(is_dir($source)) {
            if ($dest[strlen($dest)-1]=='/') {
                if ($source[strlen($source)-1]=='/') {
                    //Copy only contents
                } else {
                    //Change parent itself and its contents
                    $dest=$dest.basename($source);
                    @mkdir($dest);
                    chmod($dest,$options['filePermission']);
                }
            } else {
                if ($source[strlen($source)-1]=='/') {
                    //Copy parent directory with new name and all its content
                    @mkdir($dest,$options['folderPermission']);
                    chmod($dest,$options['filePermission']);
                } else {
                    //Copy parent directory with new name and all its content
                    @mkdir($dest,$options['folderPermission']);
                    chmod($dest,$options['filePermission']);
                }
            }

            $dirHandle=opendir($source);
            while($file=readdir($dirHandle))
            {
                if($file!="." && $file!="..")
                {
                    if(!is_dir($dirsource."/".$file)) {
                        $__dest=$dest."/".$file;
                    } else {
                        $__dest=$dest."/".$file;
                    }
                    //echo "$source/$file ||| $__dest<br />";
                    $result=$this->smartCopy($source."/".$file, $__dest, $options);
                }
            }
            closedir($dirHandle);
           
        } else {
            $result=false;
        }
        return $result;
    }

	public function msqli_db_crear_conexion($url)
	{
		$out = fopen($url."conexion.php", "w+");
		fwrite($out, "<?php
		    \$con=@mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);
		    if(!\$con){
		        die('imposible conectarse: '.mysqli_error(\$con));
		    }
		    if (@mysqli_connect_errno()) {
		        die('Conexión falló: '.mysqli_connect_errno().' : '. mysqli_connect_error());
		    }
		");
		fwrite($out, "
		    function mes(\$m)
			{
				switch (\$m)
				{
					case '01':
					case '1': 
						return 'ENERO'; 
						break;
					case '02':
					case '2': 
						return 'FEBRERO'
						; break;
					case '03':
					case '3': 
						return 'MARZO'; 
						break;
					case '04':
					case '4': 
						return 'ABRIL'; 
						break;
					case '05':
					case '5': 
						return 'MAYO';
						break;
					case '06':
					case '6': 
						return 'JUNIO'; 
						break;
					case '07':
					case '7': 
						return 'JULIO'; 
						break;
					case '08':
					case '8': 
						return 'AGOSTO';
						 break;
					case '09':
					case '9': 
						return 'SEPTIEMBRE';
						break;
					case '10':
					case '10': 
						return 'OCTUBRE';
						break;
					case '11':
					case '11': 
						return 'NOVIEMBRE';
						break;
					case '12':
					case '12': 
						return 'DICIEMBRE'; 
						break;	
					default:
						return '123e23234';
						break;
				}
			}
			");

			fwrite($out, "
			function periodo(\$fini,\$ffin)
			{
				\$conv=[];
				\$conv=explode('-', \$fini);
				\$aux=\$conv[2].' DE '.mes(\$conv[1]).' DEL '.\$conv[0].' AL ';
				\$conv=[];
				\$conv=explode('-', \$ffin);
				\$aux.=\$conv[2].' DE '.mes(\$conv[1]).' DEL '.\$conv[0];
				return \$aux;
			}
			
			function fecha(\$fini)
			{
				\$conv=[];
				\$conv=explode('-', \$fini);
				\$aux=\$conv[2].' DE '.mes(\$conv[1]).' DEL '.\$conv[0];
				return \$aux;

			}
			function convert_date(\$date)
			{
				\$fecha=explode('/', \$date);
				return \$fecha[2].'-'.\$fecha[1].'-'.\$fecha[0];
			}


			function date_format1(\$datetime)
			{

				\$fecha = explode('-', separar(\$datetime));
		    	\$fecha = \$fecha[1].'/'.\$fecha[2].'/'.\$fecha[0];
		    	return \$fecha;
			}
		");
		fwrite($out, "
			function date_format2(\$datetime)
			{
				//echo \$datetime;
				\$fecha = explode('-', \$datetime);
		    	\$fecha = \$fecha[2].'/'.\$fecha[1].'/'.\$fecha[0];
		    	return \$fecha;
			}

			function time_format(\$datetime)
			{
				return separar(\$datetime,'time');
			}

			function separar(\$string,\$tipo = null)
			{
				\$string = explode(' ', \$string);

				if (\$tipo == 'time')
				{
					return \$string[1];
				}
				else
				{
					return \$string[0];
				}
			}

			function pre(\$q)
			{
				echo '<pre>';
				var_dump(\$q);
				echo '</pre>';
			}

			function get_row(\$table,\$row, \$id, \$equal)
			{
				global \$con;
				\$query=mysqli_query(\$con,".'"'."select \$row from \$table where \$id='\$equal'".'"'.");
				\$rw=mysqli_fetch_array(\$query);
				\$value=\$rw[\$row];
				return \$value;
			}
			?>

			");
			fclose($out);



			$out = fopen($url."db.php", "w+");
			fwrite($out, 
			"
			<?php
			define('DB_HOST', 'localhost');
			define('DB_USER', 'root');
			define('DB_PASS', '');
			define('DB_NAME', '".$this->clave_sitio."');



			//PHP MAILER
			define('CORREOEMISOR', 'zw.und.ne@gmail.com');
			define('TIPOSEGURIDAD', 'tls');
			define('DOMINIO', 'smtp.gmail.com');
			define('PUERTO', '587');
			define('PASSWORD', 'empresazw');
			define('NOMBREEMISOR', 'Zwanzing und Neunzehn S.A.S. de C.V.');
			?>");
			fclose($out);

			$out = fopen($url.'/db.sql', "w+");		
			$header='
				DROP DATABASE IF EXISTS '.$this->clave_sitio.';
				CREATE DATABASE IF NOT EXISTS '.$this->clave_sitio.';
				SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
				SET time_zone = "+00:00"; ';
			$consulta='';
			$cadena='';
			$indice_consulta='';
			foreach ( $this->tablas as $tabla )
			{
				$cadena.='

				CREATE TABLE '.$tabla->clave_tabla.' (';
				if(isset($tabla->campos))
				{
					foreach ($tabla->campos as $campo) 
					{	
						$cadena.="`".$campo['clave_campo']."` ";
						switch (intval($campo['tipo_campo']))
						{

							case 1://INT
								$cadena.='INT('.intval($campo['longitud_campo']).')';if($campo['requerido']==1){$cadena.=' NOT NULL, ';}else{$cadena.=", ";}
								break;

							case 2://VARCHAR
								$cadena.='VARCHAR('.intval($campo['longitud_campo']).")";if($campo['requerido']==1){$cadena.=' NOT NULL, ';}else{$cadena.=", ";}
								break;

							case 3://DATE
								$cadena.='DATE ';if($campo['requerido']==1){$cadena.=' NOT NULL, ';}else{$cadena.=", ";}
								$ndate=1;
								break;

							case 4://DATETIME
								$cadena.='DATETIME ';if($campo['requerido']==1){$cadena.=' NOT NULL, ';}else{$cadena.=", ";}
								$ndate=1;
								break;

							case 5://TEXT
								$cadena.='VARCHAR('.intval($campo['longitud_campo']).")";if($campo['requerido']==1){$cadena.=' NOT NULL, ';}else{$cadena.=", ";}
								break;

							case 6://FILE
								$cadena.='VARCHAR('.intval($campo['longitud_campo']).")";if($campo['requerido']==1){$cadena.=' NOT NULL, ';}else{$cadena.=", ";}
								break;

							case 7://BOOLEAN
								$cadena.='TINYINT(1)';if($campo['requerido']==1){$cadena.=' NOT NULL, ';}else{$cadena.=", ";}
								break;

							case 8://DOUBLE
								$cadena.='DOUBLE ';if($campo['requerido']==1){$cadena.=' NOT NULL, ';}else{$cadena.=", ";}
								break;

							default:
								# code...
								break;
						}
					}
				}

				if($tabla->tipo_tabla==1)
				{
					foreach ($tabla->indice as $campo )
					{
						$cadena.="`".$campo['clave_campo']."` ";
						$cadena.='INT('.intval($campo['longitud_campo']).')';if($campo['requerido']==1){$cadena.=' NOT NULL, ';}else{$cadena.=", ";}
						$indice_consulta.='
						ALTER TABLE `'.$tabla->clave_tabla.'` ADD PRIMARY KEY (`'.$campo['clave_campo'].'`), ADD KEY `'.$campo['clave_campo'].'` (`'.$campo['clave_campo'].'`);
						ALTER TABLE `'.$tabla->clave_tabla.'` MODIFY `'.$campo['clave_campo'].'` int('.$campo['longitud_campo'].')  NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=0;';
					}
				}
				else if($tabla->tipo_tabla==2)
				{
					foreach ($tabla->indice as $campo )
					{
						$cadena.="`".$campo['clave_campo']."` ";
						$cadena.='INT('.intval($campo['longitud_campo']).')';if($campo['requerido']==1){$cadena.=' NOT NULL, ';}else{$cadena.=", ";}
						$indice_consulta.='
						ALTER TABLE `'.$tabla->clave_tabla.'` ADD PRIMARY KEY (`'.$campo['clave_campo'].'`), ADD KEY `'.$campo['clave_campo'].'` (`'.$campo['clave_campo'].'`);
						ALTER TABLE `'.$tabla->clave_tabla.'` MODIFY `'.$campo['clave_campo'].'` int('.$campo['longitud_campo'].')  NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=0;';
					}
					if(isset($tabla->indices_externos))
					{
						foreach ($tabla->indices_externos as $campo )
						{
							$cadena.="`".$tabla->clave_tabla.'_'.$campo['clave_campo']."_".$tabla->nombre_origen."` ";
							$cadena.='INT('.intval($campo['longitud_campo']).')';if($campo['requerido']==1){$cadena.=' NOT NULL, ';}else{$cadena.=", ";}
						}
					}
				}
				$cadena=substr($cadena, 0, -2).") ENGINE=InnoDB DEFAULT CHARSET=utf8;".$indice_consulta;
				$indice=array('');

				$consulta.=$cadena;
				$cadena='';
				$indice_consulta='';
			}
				fwrite($out, $header.$consulta.PHP_EOL);
				if($this->usuarios==1)
				{
					fwrite($out, "
				CREATE TABLE `lugares` (
				  `id` int(11) NOT NULL,
				  `id_session` int(11) NOT NULL,
				  `ip` varchar(20) NOT NULL,
				  `fecha` datetime NOT NULL
				) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

				CREATE TABLE `usuarios` (
				  `id` int(11) NOT NULL,
				  `password` varchar(130) NOT NULL,
				  `nombre` varchar(30) NOT NULL,
				  `appater` varchar(30) NOT NULL,
				  `apmater` varchar(30) NOT NULL,
				  `correo` varchar(80) NOT NULL,
				  `activacion` int(11) NOT NULL DEFAULT 0,
				  `token` varchar(40) NOT NULL,
				  `token_password` varchar(100) DEFAULT NULL,
				  `password_request` int(11) DEFAULT 0,
				  `tokensesion` varchar(100) NOT NULL
				) ENGINE=InnoDB DEFAULT CHARSET=utf8;

				INSERT INTO `usuarios` (`id`, `password`, `nombre`, `appater`, `apmater`, `correo`, `activacion`, `token`, `token_password`, `password_request`, `tokensesion`) VALUES
				(0, '$2y$10$TWocg6qXn7p2b0P4LM3HBe/raypuqF79NVVfN//oPQ60BdCMhMu5O', 'Juan', 'Perez', 'Cid', 'admin@admin.com', 1, '1b0b63d725e598e48cc88ebe036b172c', '', 0, '');
				ALTER TABLE `lugares`
				  ADD PRIMARY KEY (`id`);
				ALTER TABLE `usuarios`
				  ADD PRIMARY KEY (`id`);
				ALTER TABLE `lugares`
				  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=0;
				ALTER TABLE `usuarios`
				  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;");
				}
			fclose($out);
	}

	public function templates($url)
	{
		$out = fopen($url.'/head.php', "w+");
		fwrite($out,'
		<?php header("Access-Control-Allow-Origin", "*"); 

		session_start();
	    if(isset($_SESSION["tiempo"]) ) {

	        $inactivo = 1200;//20min en este caso.

	        $vida_session = time() - $_SESSION["tiempo"];

	            if($vida_session > $inactivo)
	            {
	                //Removemos sesión.
	                session_unset();
	                //Destruimos sesión.
	                session_destroy();              
	                //Redirigimos pagina.
	                header("Location: index.php");
	                exit();
	            }

	    }
	    $_SESSION["tiempo"] = time();
	    //echo "<pre>";
	    //var_dump($_SESSION);
	    //echo "</pre>";

	    ?>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<title><?php echo $title;?></title>
		<style>"<?php include(dirname(__FILE__). "./css/bootstrap.min.css"); ?>"</style>
		<style>"<?php include(dirname(__FILE__). "./css/all.min.css"); ?>"</style>
		<style sizes="32x32" type="image/png">"<?php include(dirname(__FILE__). "./img/logo-icon.png"); ?>"</style>
		<style>"<?php include(dirname(__FILE__). "./css/datepicker.css"); ?>" type="text/css"</style>
		');
		fclose($out);

		$out = fopen($url.'/footer.php', "w+");
		fwrite($out,'
		<div class="navbar navbar-default navbar-fixed-bottom">
			<div class="container">
				<p class="navbar-text pull-left"><?php echo date("Y");?>
					<a href="#" target="_blank" style="color: #ecf0f1">Control de obra</a>
				</p>
			</div>
		</div>
		<script><?php include(dirname(__FILE__). "./js/jquery.min.js"); ?></script>
		<script><?php include(dirname(__FILE__). "./js/bootstrap.min.js"); ?></script>
		<script><?php include(dirname(__FILE__). "./js/all.min.js"); ?></script>
		');
		fclose($out);

		$out = fopen($url.'/navbar0.php', "w+");
		fwrite($out,'
		<?php
		if (isset($title))
		{
		?>
		<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
			<a class="navbar-brand" href="index.php">'.$this->clave_sitio.'</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample02" aria-controls="navbarsExample02" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarsExample02">
			<ul class="navbar-nav mr-auto">
			');
			foreach ($this->tablas as $tabla )
			{
				if($tabla->tipo_tabla==1)
				fwrite($out, '
				<li class="nav-item <?php echo $active'.$tabla->clave_tabla.';?>"><a class="nav-link" href="'.$tabla->clave_tabla.'/"><span class="'.$tabla->icono($tabla->id_icono).'"></span> '.$tabla->clave_tabla.'</a></li>');
			}
			fwrite($out, '</ul>');
			fwrite($out, '
				<!--form class="form-inline my-2 my-md-0">
					<input class="form-control" type="text" placeholder="Busqueda">
				</form-->
				<ul class="nav navbar-nav navbar-right">
					<li><a class="nav-link" href="../logout.php"><i class="fas fa-xs fa-power-off"></i> Salir</a></li>
				</ul>
			</div>
		</nav>


		</div>
		</div>
		</nav>
		<?php
		}
		?>');
		fclose($out);

		$out = fopen($url.'/navbar1.php', "w+");
		fwrite($out,'
		<?php
		if (isset($title))
		{
		?>
		<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
			<a class="navbar-brand" href="index.php">'.$this->clave_sitio.'</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample02" aria-controls="navbarsExample02" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarsExample02">
			<ul class="navbar-nav mr-auto">
			');
			foreach ($this->tablas as $tabla )
			{
				if($tabla->tipo_tabla==1)
				fwrite($out, '
				<li class="nav-item <?php echo $active'.$tabla->clave_tabla.';?>"><a class="nav-link" href="../'.$tabla->clave_tabla.'/"><span class="'.$tabla->icono($tabla->id_icono).'"></span> '.$tabla->clave_tabla.'</a></li>');
			}
			fwrite($out, '</ul>');
			fwrite($out, '
				<!--form class="form-inline my-2 my-md-0">
					<input class="form-control" type="text" placeholder="Busqueda">
				</form-->
				<ul class="nav navbar-nav navbar-right">
					<li><a class="nav-link" href="../logout.php"><i class="fas fa-xs fa-power-off"></i> Salir</a></li>
				</ul>
			</div>
		</nav>


		</div>
		</div>
		</nav>
		<?php
		}
		?>');
		fclose($out);

		$out = fopen($url.'/navbar2.php', "w+");
		fwrite($out,'
		<?php
		if (isset($title))
		{
		?>
		<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
			<a class="navbar-brand" href="index.php">'.$this->clave_sitio.'</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample02" aria-controls="navbarsExample02" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarsExample02">
			<ul class="navbar-nav mr-auto">
			');
			foreach ($this->tablas as $tabla )
			{
				if($tabla->tipo_tabla==1)
				fwrite($out, '
				<li class="nav-item <?php echo $active'.$tabla->clave_tabla.';?>"><a class="nav-link" href="../../'.$tabla->clave_tabla.'/"><span class="'.$tabla->icono($tabla->id_icono).'"></span> '.$tabla->clave_tabla.'</a></li>');
			}
			fwrite($out, '</ul>');
			fwrite($out, '
				<!--form class="form-inline my-2 my-md-0">
					<input class="form-control" type="text" placeholder="Busqueda">
				</form-->
				<ul class="nav navbar-nav navbar-right">
					<li><a class="nav-link" href="../logout.php"><i class="fas fa-xs fa-power-off"></i> Salir</a></li>
				</ul>
			</div>
		</nav>


		</div>
		</div>
		</nav>
		<?php
		}
		?>');
		fclose($out);
	}

	public function nuevacarpeta($url, $index=true)
	{
		if(!file_exists($url))
			mkdir($url, 0700);
		if($index)
		{
			$out = fopen($url."/"."index.php", "w+");
				fwrite($out,'<?php header("location: ../"); ?>');
			fclose($out);
		}	
	}

	public function quematodoyhuye($url)
	{
	    $dir = opendir($url);
	    while(false !== ( $file = readdir($dir)) )
	    {
	        if (( $file != '.' ) && ( $file != '..' ))
	        {
	            $full = $url . '/' . $file;
	            if ( is_dir($full) )
	            {
	                $this->quematodoyhuye($full);
	            }
	            else
	            {
	                unlink($full);
	            }
	        }
	    }
	    closedir($dir);
	    rmdir($url);
	}

	public function index_modal_principal($url, $tabla1)
	{
		$out = fopen($url.'/index.php', "w+");
		fwrite($out,'
		<?php
		require_once ("../config/db.php");
		require_once ("../config/conexion.php");');


		foreach ( $this->tablas as $tabla )
		{
			if($tabla->clave_tabla==$tabla1->clave_tabla)
			{
				fwrite($out, '
				$active'.$tabla1->clave_tabla.'="active";');
			}
			else
			{
				fwrite($out, '
				$active'.$tabla1->clave_tabla.'="";');
			}
		}
		fwrite($out,'
		$title="'.$tabla1->clave_tabla.' | '.$this->clave_sitio.'";
		?>


		<!DOCTYPE html>
		<html lang="es">
		  <head>
			<?php include("../head.php");?>
		  </head>
		  <body>
		 	<?php include("../navbar1.php");?> 
		    <div class="container">
		    	<br>
				<div class="panel panel-default">
					<div class="panel-heading">
						<div class="row">
						</div>
					</div>			
					<div class="panel-body">
					<?php
						include("../modal/'.$tabla1->clave_tabla.'_add.php");
						include("../modal/'.$tabla1->clave_tabla.'_edit.php");
					?>
					<div class="row">
						<form class="form-horizontal col-md-11" role="form" id="datos_preest">
							<div class="form-group row">
								<div class="col-md-10">
									<input type="text" class="form-control" id="q" placeholder="Nombre" onkeyup="load(1);">
								</div>
								<div class="col-md-2">
									<button type="button" class="btn btn-primary" onclick="load'.$tabla1->clave_tabla.'(1);"><span class="fas fa-xs fa-search" ></span> Buscar</button>
								</div>
							</div>
						</form>
					    <div class="col-md-1">
							<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#'.$tabla1->clave_tabla.'_modal_add"><span class="fas fa-xs fa-plus" ></span></button>
						</div>
					</div>
					<span id="loader"></span>
					<div id="resultados'.$tabla1->clave_tabla.'"></div>
					<div class="outer_div'.$tabla1->clave_tabla.'"></div>
					</div>
				</div>
			</div>
			<hr>
			<?php
				include("../footer.php");
			?>
			<script type="text/javascript" src="../js/'.$tabla1->clave_tabla.'.js"></script>
		  </body>
		</html>

		');
		fclose($out);
	}

	public function index_Ver($url, $tabla1)
	{

		foreach ($tabla1->indice as $campo)
		{
			if($campo['indice']==1)
			{
				$orderby=$campo['clave_campo'];
				break;
			}
		}
		$out = fopen($url.'/ver.php', "w+");
		fwrite($out,'
		<?php
		if(!isset($_GET["'.$tabla1->clave_tabla.'"]))
		{
			header("location: index.php");
		}
		else
		{
			$'.$tabla1->clave_tabla.'=$_GET["'.$tabla1->clave_tabla.'"];
		}


		require_once ("../config/db.php");
		require_once ("../config/conexion.php");

		$consulta=mysqli_query($con,"SELECT * FROM '.$tabla1->clave_tabla.' WHERE '.$orderby.'=$'.$tabla1->clave_tabla.';");
		$row=mysqli_fetch_array($consulta);

		');


		foreach ( $this->tablas as $tabla )
		{
			if($tabla->clave_tabla==$tabla1->clave_tabla)
			{
				fwrite($out, '
				$active'.$tabla1->clave_tabla.'="active";');
			}
			else
			{
				fwrite($out, '
				$active'.$tabla1->clave_tabla.'="";');
			}
		}
		fwrite($out,'
		$title="'.$tabla1->clave_tabla.' | '.$this->clave_sitio.'";
		?>

		<!DOCTYPE html>
		<html lang="es">
		  <head>
			<?php include("../head.php");?>
		  </head>
		  <body>
		 	<?php include("../navbar1.php");?> 
		    <div class="container">
		    	<br>
				<div class="panel panel-default">
					<div class="panel-heading">
						<div class="row">
						</div>
					</div>			
					<div class="panel-body">
					<?php
						');

			if(isset($tabla1->subtablas))
			{
				foreach ($tabla1->subtablas as $tabla2)
				{
					fwrite($out,'
							include("../modal/'.$tabla2['clave_tabla'].'_add.php");
							include("../modal/'.$tabla2['clave_tabla'].'_edit.php");
							');
				}
			}

		fwrite($out,'
					?>');

		fwrite($out,'
					<div class="row">
					<?php
					echo "<pre>";
					//var_dump($row);
					echo "</pre>";
					?>
					</div>');
			foreach ($tabla1->campos as $campo)
			{
				if($campo['visible']==1)
				{
					fwrite($out,'
					<div class="row">');

					if($campo['visible']==1)
					{
						if($campo['tipo_campo']==6)
						{

							fwrite($out,'<h3>'.$campo['clave_campo'].'<h3><img src="../'.$tabla1->clave_tabla.'/'.$campo['carpeta'].'/<?php echo $row["'.$campo['clave_campo'].'"]; ?>" width="100">'.PHP_EOL);
						}
						else
						{
							fwrite($out,'<h3>'.$campo['clave_campo'].': <h3><?php echo $row["'.$campo['clave_campo'].'"]; ?>'.PHP_EOL);
						}
					}
					fwrite($out,'
					</div>');
				}			
			}

			if(isset($tabla1->subtablas))
			{
				foreach ($tabla1->subtablas as $tabla2)
				{
			
					fwrite($out,'
						<h5>'.$tabla2['clave_tabla'].'</h5>
						<div class="row">
							<form class="form-horizontal col-md-11" role="form" id="datos_preest">
								<div class="form-group row">
									<div class="col-md-10">
										<input type="text" class="form-control" id="q" placeholder="Nombre" onkeyup="load'.$tabla2['clave_tabla'].'(1);">
									</div>
									<div class="col-md-2">
										<button type="button" class="btn btn-primary" onclick="load'.$tabla2['clave_tabla'].'(1);"><span class="fas fa-xs fa-search" ></span> Buscar</button>
									</div>
								</div>
							</form>
						    <div class="col-md-1">
								<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#'.$tabla2['clave_tabla'].'_modal_add"><span class="fas fa-xs fa-plus" ></span></button>
							</div>
						</div>
						<span id="loader"></span>
						<div id="resultados'.$tabla2['clave_tabla'].'"></div>
						<div class="outer_div'.$tabla2['clave_tabla'].'"></div>
						');
				}
			}



					fwrite($out, '
				</div>
			</div>
			<hr>
			<?php
				include("../footer.php");
			?>
			');

			if(isset($tabla1->subtablas))
			{
				foreach ($tabla1->subtablas as $tabla2) 
				{
					fwrite($out,'
					<script type="text/javascript" src="../js/'.$tabla2['clave_tabla'].'.js"></script>');
				}
			}
			fwrite($out,'
		  </body>
		</html>

		');
		fclose($out);
	}
	
}
?>
